package ass03.es1;

import java.util.List;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.01
 *
 * Classe che modella una tupla (parola,lista di posizioni)
 */
public interface WordPos {
    String getWord();
    List<Integer> getPos();
}
