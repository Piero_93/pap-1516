package ass03.es1;

import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.01
 *
 * Implementare in Java la classe TextUtil come libreria che fornisce le seguenti funzioni (implementate come metodi statici),
 * fornendo soluzioni che evitano l’uso di cicli e si basano sull’uso di Stream API e lambda expression.
 */
public class TextUtil {
    /**
     * Assigment 3.01.1
     * @param text
     * @return determina la lista delle parole e la relativa lunghezza
     */
    public static List<WordLen> getWordsLength(String text) {
        Stream<String> s = Arrays.asList(text.split("\\W+")).stream();
        return s.filter(w -> w.length() != 0)
                .map(word -> new WordLen(word, word.length()))
                .collect(Collectors.toList());
    }

    /**
     * Assigment 3.01.2
     * @param text
     * @return determina la parola di lunghezza maggiore.
     *
     * Nota: nel caso ci siano più parole con lunghezza uguale verrà restituita la prima trovata.
     */
    public static Optional<String> getWordWithMaxLen(String text) {
        Stream<String> s = Arrays.asList(text.split("\\W+")).stream();
        return s.filter(w -> w.length() != 0).max(Comparator.comparing(String::length));
    }

    /**
     * Assigment 3.01.3
     * @param text
     * @param word
     * @return determina la frequenza (numero occorrenze) della parola nel testo specificato.
     *
     * Nota: parole che differiscono solo per il caso delle lettere verranno considerate diverse.
     * Per cambiare il comportamento è sufficiente sostituire "compareTo" con "compareToIgnoreCase".
     */
    public static int getWordFreq(String text, String word) {
        Stream<String> s = Arrays.asList(text.split("\\W+")).stream();
        return (int) s.filter(w -> w.length() != 0 && w.compareTo(word) == 0)
                .count();
    }

    /**
     * Assigment 3.01.4
     * @param text
     * @return la lista delle posizioni in cui ogni parola del testo compare.
     *
     * Nota: parole che differiscono solo per il caso delle lettere verranno considerate diverse.
     */
    public static List<WordPos> getWordsPos(String text) {
        String[] words = text.split("\\W+");
        return IntStream.range(0, words.length)
                .filter(i -> !words[i].isEmpty())
                .mapToObj(i -> new WordWithIndex(words[i], i + 1))
                .reduce(
                        new HashMap<String, Set<Integer>>(),
                        //Aggiunge l'indice della parola corrente alla mappa
                        (map, current) -> {
                            //Se la parola corrente non è mappata, crea il record con il set vuoto.
                            //Restituisce il set (già esistene o appena creato) e aggiunge l'indice della parola corrente.
                            map.computeIfAbsent(current.getWord(), k -> new TreeSet<>()).add(current.getIndex());
                            return map;
                        },
                        //Unisce le mappe a e b
                        (a , b) -> {
                            //Per ogni elemento della seconda mappa, lo aggiunge alla prima
                            b.forEach((x, y) -> a.computeIfAbsent(x, k -> new TreeSet<>()).addAll(y));
                            return a;
                        }
                ).entrySet()
                .stream()
                .map((entry) -> new WordPosImpl(entry.getKey(), new ArrayList<>(entry.getValue())))
                .collect(Collectors.toList());
    }

    /**
     * Classe di utilità che modella tuple (parola,indice), utilizzata nel metodo "getWordPos"
     */
    static class WordWithIndex {
        private String word;
        private int index;

        public WordWithIndex(String word, int index) {
            this.word = word;
            this.index = index;
        }

        public String getWord() {
            return word;
        }

        public int getIndex() {
            return index;
        }
    }
}
