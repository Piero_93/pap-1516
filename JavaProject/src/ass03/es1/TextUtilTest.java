package ass03.es1;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ass03.es1.TextUtil.*;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 01 (per 2016-04-08)
 *
 * Classe di testing per TextUtil
 */
public class TextUtilTest {
    /*
     * Esempi di stringhe in input
     */
    private final static String complexText = "aa.BB!.,-!!cc,12'EEE FFF-AA-aa aa aa BB EEE a ! f";
    private final static String emptyText = "";

    /**
     * Testing del metodo "getWordsLenght"
     */
    @Test
    public void testGetWordsLenght() {
        //Output atteso dal metodo "getWordsLenghtOutput" con "complexText" in input
        List<WordLen> complexTextGetWordsLengthOutput = new ArrayList<>(Arrays.asList(
                new WordLen[]{
                        new WordLen("aa", 2),
                        new WordLen("BB", 2),
                        new WordLen("cc", 2),
                        new WordLen("12", 2),
                        new WordLen("EEE", 3),
                        new WordLen("FFF", 3),
                        new WordLen("AA", 2),
                        new WordLen("aa", 2),
                        new WordLen("aa", 2),
                        new WordLen("aa", 2),
                        new WordLen("BB", 2),
                        new WordLen("EEE", 3),
                        new WordLen("a", 1),
                        new WordLen("f", 1)
                }
        ));

        assertEquals(complexTextGetWordsLengthOutput, getWordsLength(complexText));
        assertEquals(new ArrayList<WordLen>(), getWordsLength(emptyText));
    }

    /**
     * Testing del metodo "getWordWithMaxLen"
     */
    @Test
    public void testGetWordWithMaxLen() {
        assertEquals("EEE", getWordWithMaxLen(complexText).get());
        assertFalse(getWordWithMaxLen(emptyText).isPresent());
    }

    /**
     * Testing del metodo "getWordFreq"
     */
    @Test
    public void testGetWordFreq() {
        assertEquals(4, getWordFreq(complexText, "aa"));
        assertEquals(0, getWordFreq(complexText, "zz"));
        assertEquals(0, getWordFreq(complexText, ""));
        assertEquals(0, getWordFreq(emptyText, "zz"));
    }

    /**
     * Testing del metodo "getWordPos"
     */
    @Test
    public void testGetWordPos() {
        //Output atteso dal metodo "getPosOutput" con "complexText" in input
        List<WordPos> complexTextGetPosOutput = new ArrayList<>(Arrays.asList(
                new WordPosImpl("AA", new ArrayList<>(Arrays.asList(new Integer[]{7}))),
                new WordPosImpl("cc", new ArrayList<>(Arrays.asList(new Integer[]{3}))),
                new WordPosImpl("BB", new ArrayList<>(Arrays.asList(new Integer[]{2,11}))),
                new WordPosImpl("aa", new ArrayList<>(Arrays.asList(new Integer[]{1,8,9,10}))),
                new WordPosImpl("a", new ArrayList<>(Arrays.asList(new Integer[]{13}))),
                new WordPosImpl("12", new ArrayList<>(Arrays.asList(new Integer[]{4}))),
                new WordPosImpl("EEE", new ArrayList<>(Arrays.asList(new Integer[]{5,12}))),
                new WordPosImpl("f", new ArrayList<>(Arrays.asList(new Integer[]{14}))),
                new WordPosImpl("FFF", new ArrayList<>(Arrays.asList(new Integer[]{6})))
        ));

        assertEquals(complexTextGetPosOutput, getWordsPos(complexText));
        assertEquals(new ArrayList<WordPos>(), getWordsPos(emptyText));
    }
}
