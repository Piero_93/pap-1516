package ass03.es1;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.01
 *
 * Classe che modella una tupla (parola, lunghezza).
 * Per poter creare i test JUnit, è stato fatto override del metodo equals.
 */
public class WordLen {
    final private String word;
    final private int len;

    public WordLen(String w, int l){
        this.word = w;
        this.len = l;
    }

    public String getWord(){
        return word;
    }

    public int getLength(){
        return len;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof WordLen) {
            WordLen toBeCompared = (WordLen) obj;
            if (toBeCompared.getWord().equals(this.getWord()) &&
                    toBeCompared.getLength() == this.getLength()) {
                return true;
            }
        }
        return false;
    }
}
