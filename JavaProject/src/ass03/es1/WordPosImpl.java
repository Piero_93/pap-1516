package ass03.es1;

import java.util.ArrayList;
import java.util.List;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.01
 *
 * Implementazione dell'interfaccia WordPos
 */
public class WordPosImpl implements WordPos {
    private String word;
    private List<Integer> pos = new ArrayList<>();

    public WordPosImpl(String word, List<Integer> pos) {
        this.word = word;
        this.pos = pos;
    }

    @Override
    public String getWord() {
        return word;
    }

    @Override
    public List<Integer> getPos() {
        return pos;
    }

    @Override
    public String toString() {
        return "(" + word + "->" + pos + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof  WordPos) {
            WordPos toBeCompared = (WordPos) obj;
            if(toBeCompared.getWord().equals(this.getWord()) &&
                    toBeCompared.getPos().equals(this.getPos())) {
                return true;
            }
        }
        return false;
    }
}
