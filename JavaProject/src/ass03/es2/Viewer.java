package ass03.es2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.02
 *
 * Modella la GUI che permette di visualizzare i punti ed evidenziare una regione.
 * Contiene il metodo "main" che permette di visualizzare la finestra.
 * Il sistema di riferimento prevede l'origine in alto a sinistra e gli assi che crescono verso destra e verso il basso.
 */
public class Viewer extends JFrame {
    /*
     * Fattore di scala utilizzato per ingrandire la dimensione degli elementi da visualizzare
     */
    private static final double SCALE_FACTOR = 10.0;

    /*
     * Insieme dei punti da visualizzare
     */
    private final PointCloud cloud;

    /*
     * Regione attualmente selezionata. Inizialmente è selezionata una regione compresa tra (0,0) e (0,0)
     */
    private Optional<Region> selectedRegion;
    private Optional<P2d> currentPoint;


    /**
     * @param x larghezza del pannello
     * @param y altezza del pannello
     * @param c insieme dei punti da visualizzare
     */
    public Viewer(double x, double y, PointCloud c) {
        this.cloud = c;
        this.setTitle("Point Cloud Viewer");
        this.setSize(new Dimension((int) (x * SCALE_FACTOR + 100), (int) (y * SCALE_FACTOR + 100)));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.selectedRegion = Optional.empty();
        this.currentPoint = Optional.empty();

        //Aggiunta del pannello al frame
        this.getContentPane().add(new JPanel() {
            /**
             * Metodo che si occupa di disegnare gli oggetti sul pannello
             * @param g
             */
            @Override
            public void paint(Graphics g) {
                super.paint(g);

                g.setFont(new Font("Arial", Font.PLAIN, 10));
                if(selectedRegion.isPresent()) {
                    //Rappresentazione della regione selezionata (rettangolo rosso e coordinate selezionate rosse)
                    g.setColor(Color.red);
                    int x = (int) Math.round(selectedRegion.get().getUpperLeft().getX() * SCALE_FACTOR);
                    int y = (int) Math.round(selectedRegion.get().getUpperLeft().getY() * SCALE_FACTOR);
                    int width = (int) Math.abs(selectedRegion.get().getUpperLeft().getX() * SCALE_FACTOR - selectedRegion.get().getBottomRight().getX() * SCALE_FACTOR);
                    int height = (int) Math.abs(selectedRegion.get().getUpperLeft().getY() * SCALE_FACTOR - selectedRegion.get().getBottomRight().getY() * SCALE_FACTOR);

                    g.drawRect(x, y, width, height);
                    g.drawString("Selected region: " +
                            "(" + (int) selectedRegion.get().getUpperLeft().getX() + ", " + (int) selectedRegion.get().getUpperLeft().getY() + ") | " +
                            "(" + (int) selectedRegion.get().getBottomRight().getX() + ", " + (int) selectedRegion.get().getBottomRight().getY() + ")", x, y - 5);
                }

                //Rappresentazione dei punti sul piano (rosso = nella regione selezionata, blu = fuori dalla regione selezionata)
                cloud.getPointsInRegion(
                        new Region(new P2d(0, 0), new P2d(this.getWidth() / SCALE_FACTOR + SCALE_FACTOR, this.getHeight() / SCALE_FACTOR + SCALE_FACTOR))).stream()
                        .forEach(point -> {
                            if (selectedRegion.isPresent() && cloud.getPointsInRegion(selectedRegion.get()).contains(point)) {
                                g.setColor(Color.red);
                            } else {
                                g.setColor(Color.blue);
                            }
                            g.fillOval(
                                    (int) Math.round(point.getX() * SCALE_FACTOR - SCALE_FACTOR / 2),
                                    (int) Math.round(point.getY() * SCALE_FACTOR - SCALE_FACTOR / 2),
                                    (int) SCALE_FACTOR,
                                    (int) SCALE_FACTOR
                            );
                            g.drawString("(" + (int) point.getX() + ", " + (int) point.getY() + ")",
                                    (int) (point.getX() * SCALE_FACTOR + SCALE_FACTOR), (int) (point.getY() * SCALE_FACTOR));
                        });

                if(currentPoint.isPresent()) {
                    //Indicazione del punto più vicino (aggiuntivo, non specificato nella consegna)
                    Optional<P2d> nearestPoint = cloud.getNearestPoint(currentPoint.get());

                    if(nearestPoint.isPresent()) {
                        g.setColor(Color.magenta);
                        g.drawString(
                                "D: " + new DecimalFormat("#.##").format(nearestPoint.get().distance(nearestPoint.get(), currentPoint.get())),
                                (int) Math.round(currentPoint.get().getX() * SCALE_FACTOR + 4),
                                (int) Math.round(currentPoint.get().getY() * SCALE_FACTOR)
                        );
                        g.drawLine(
                                (int) Math.round(nearestPoint.get().getX() * SCALE_FACTOR),
                                (int) Math.round(nearestPoint.get().getY() * SCALE_FACTOR),
                                (int) Math.round(currentPoint.get().getX() * SCALE_FACTOR),
                                (int) Math.round(currentPoint.get().getY() * SCALE_FACTOR)
                        );
                    }
                }
            }
        });

        this.setVisible(true);

        //Listener utilizzato per rilevare gli eventi del mouse
        MouseAdapter listener = new MouseAdapter() {
            private P2d startPoint;

            @Override
            public void mouseExited(MouseEvent e) {
                currentPoint = Optional.empty();
                repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                startPoint = new P2d(e.getX() / SCALE_FACTOR, e.getY() / SCALE_FACTOR);
                currentPoint = Optional.of(startPoint);
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                P2d endPoint = new P2d(e.getX() / SCALE_FACTOR, e.getY() / SCALE_FACTOR);
                if (endPoint.equals(startPoint)) {
                    selectedRegion = Optional.empty();
                    repaint();
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                currentPoint = Optional.empty();
                P2d endPoint = new P2d(e.getX() / SCALE_FACTOR, e.getY() / SCALE_FACTOR);

                double upperLeftX;
                double upperLeftY;
                double bottomRightX;
                double bottomRightY;

                if (startPoint.getX() < endPoint.getX()) {
                    upperLeftX = startPoint.getX();
                    bottomRightX = endPoint.getX();
                } else {
                    upperLeftX = endPoint.getX();
                    bottomRightX = startPoint.getX();
                }

                if (startPoint.getY() < endPoint.getY()) {
                    upperLeftY = startPoint.getY();
                    bottomRightY = endPoint.getY();
                } else {
                    upperLeftY = endPoint.getY();
                    bottomRightY = startPoint.getY();
                }
                selectedRegion = Optional.of(new Region(new P2d(upperLeftX, upperLeftY), new P2d(bottomRightX, bottomRightY)));

                repaint();
            }
        };

        this.getContentPane().addMouseMotionListener(listener);
        this.getContentPane().addMouseListener(listener);

        JOptionPane.showMessageDialog(
                this,
                "L'origine è in alto a sinistra.\n" +
                        "Gli assi crescono verso destra e verso il basso.\n",
                "Sistema di riferimento",
                JOptionPane.INFORMATION_MESSAGE
        );
    }

    /**
     * Metodo main utilizzato per avviare la finestra modellata da questa classe.
     * @param args
     */
    public static void main(String[] args) {
        final List<P2d> cloud = new ArrayList<>();
        IntStream.range(0, 15).forEach(unused -> cloud.add(new P2d(2 + Math.random() * 46, 2 + Math.random() * 46)));
        final PointCloud myCloud = new MyPointCloud(cloud);

        new Viewer(50, 50, myCloud);
    }
}