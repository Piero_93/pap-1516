package ass03.es2;

import java.util.List;
import java.util.Optional;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.02
 *
 * Interfaccia PointCloud, modella un insieme di punti su un piano
 */
public interface PointCloud {
    /**
     * Trasla l’insieme dei punti che fanno parte del point cloud del vettore specificato
     * @param v
     */
    void move(V2d v);

    /**
     * @param r
     * @return la lista dei punti che cadono all’interno della regione
     */
    List<P2d> getPointsInRegion(Region r);

    /**
     * @param p
     * @return determina, se presente, il punto più vicino (secondo distanza euclidea) al punto specificato come parametro
     */
    Optional<P2d> getNearestPoint(P2d p);

    /**
     * @return ottiene una rappresentazione testuale dell’insieme dei punti nel formato { p1, p2, … pn }
     */
    String toString();
}
