package ass03.es2;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.02
 *
 * Modella una regione del piano.
 */
public class Region {

    private P2d upperLeft, bottomRight;

    public Region(P2d upperLeft, P2d bottomRight) {
        this.upperLeft = upperLeft;
        this.bottomRight = bottomRight;
    }

    public P2d getUpperLeft() {
        return upperLeft;
    }

    public P2d getBottomRight() {
        return bottomRight;
    }

    @Override
    public String toString() {
        return "Region(" +
                "topLeft=" + upperLeft +
                ", bottomRight=" + bottomRight +
                ')';
    }
}
