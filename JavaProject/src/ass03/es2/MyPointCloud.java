package ass03.es2;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.02
 *
 * Implementare l'interfaccia PointCloud
 */
public class MyPointCloud implements PointCloud {
    List<P2d> cloud;

    public MyPointCloud(List<P2d> cloud) {
        this.cloud = cloud;
    }

    @Override
    public void move(V2d v) {
        //Versione con stream
        //cloud = cloud.stream().map(point -> point = point.sum(v)).collect(Collectors.toList());

        //Versione con replaceAll
        cloud.replaceAll(point -> point = point.sum(v));
    }

    @Override
    public List<P2d> getPointsInRegion(Region r) {
        return cloud.stream()
                .filter(point ->
                        point.getX() >= r.getUpperLeft().getX() &&
                        point.getX() <= r.getBottomRight().getX() &&
                        point.getY() >= r.getUpperLeft().getY() &&
                        point.getY() <= r.getBottomRight().getY())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<P2d> getNearestPoint(P2d p) {
        return cloud.stream()
                .min(Comparator.comparing((a) -> P2d.distance(a, p)));
    }

    @Override
    public String toString() {
        return "{ " + cloud.stream()
                .map(point -> "(" + point.getX() + ", " + point.getY() + ")")
                .reduce("", (x, y) -> (x != "" ? (x + ", ") : x) +  y) + " }";
    }
}
