package ass03.es2;

import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 03 (per 2016-04-08)
 *
 * Assignment 3.02
 *
 * Test in JUnit per i metodi di PointCloud
 */
public class PointCloudTest {
    /**
     * Testing del metodo "toString"
     */
    @Test
    public void testToString() {
        //Cloud
        PointCloud myCloud = new MyPointCloud(new LinkedList<>(Arrays.asList(
                new P2d(28,4),
                new P2d(26,48),
                new P2d(16,19),
                new P2d(3,3),
                new P2d(58,27),
                new P2d(54,19),
                new P2d(3,44),
                new P2d(59,48),
                new P2d(36,8),
                new P2d(20,34)
        )));

        //Output atteso dal metodo "toString"
        String cloudToStringOutput = "{ (28.0, 4.0), (26.0, 48.0), (16.0, 19.0), (3.0, 3.0), " +
                "(58.0, 27.0), (54.0, 19.0), (3.0, 44.0), (59.0, 48.0), (36.0, 8.0), (20.0, 34.0) }";

        assertEquals(cloudToStringOutput, myCloud.toString());
    }

    /**
     * Testing del metodo "move" e "getPointsInRegion"
     */
    @Test
    public void testMoveAndGetPointsInRegion() {
        //Cloud
        PointCloud myCloud = new MyPointCloud(new LinkedList<>(Arrays.asList(
                new P2d(28,4),
                new P2d(26,48),
                new P2d(16,19),
                new P2d(3,3),
                new P2d(58,27),
                new P2d(54,19),
                new P2d(3,44),
                new P2d(59,48),
                new P2d(36,8),
                new P2d(20,34)
        )));

        //Vettore
        V2d vec = new V2d(1,1);

        //Regione che comprende tutti i punti
        Region all = new Region(new P2d(0,0), new P2d(100,100));

        //Regione che comprende solo alcuni punti
        Region restricted = new Region(new P2d(0,0), new P2d(50,50));

        //Output atteso dal metodo "getAllPointsInRegion" con la regione "all" (tutti i punti)
        List<P2d> getAllPointsInRegionAllOutput = new LinkedList<>(Arrays.asList(
                new P2d(29,5),
                new P2d(27,49),
                new P2d(17,20),
                new P2d(4,4),
                new P2d(59,28),
                new P2d(55,20),
                new P2d(4,45),
                new P2d(60,49),
                new P2d(37,9),
                new P2d(21,35)
        ));

        //Output atteso dal metodo "getAllPointsInRegion" con la regione "restricted" (solo alcuni punti)
        List<P2d> getAllPointsInRegionRestrictedOutput = new LinkedList<>(Arrays.asList(
                new P2d(29,5),
                new P2d(27,49),
                new P2d(17,20),
                new P2d(4,4),
                new P2d(4,45),
                new P2d(37,9),
                new P2d(21,35)
        ));

        myCloud.move(vec);
        assertEquals(getAllPointsInRegionAllOutput, myCloud.getPointsInRegion(all));
        assertEquals(getAllPointsInRegionRestrictedOutput, myCloud.getPointsInRegion(restricted));
    }

    /**
     * Testing del metodo "getNearestPoint"
     */
    @Test
    public void testGetNearestPoint() {
        //Cloud
        PointCloud myCloud = new MyPointCloud(new LinkedList<>(Arrays.asList(
                new P2d(28,4),
                new P2d(26,48),
                new P2d(16,19),
                new P2d(3,3),
                new P2d(58,27),
                new P2d(54,19),
                new P2d(3,44),
                new P2d(59,48),
                new P2d(36,8),
                new P2d(20,34)
        )));

        //Punto
        P2d point = new P2d(0,0);

        assertEquals(new P2d(3,3), myCloud.getNearestPoint(point).get());
        assertFalse(new MyPointCloud(new LinkedList<>()).getNearestPoint(point).isPresent());
    }
}
