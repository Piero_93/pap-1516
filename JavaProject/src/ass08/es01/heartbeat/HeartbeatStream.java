package ass08.es01.heartbeat;

import pap.ass08.pos.Flag;
import rx.Subscriber;

/**
 * HeartbeatStream
 * Emette periodicamente valori di frequenza
 */
public class HeartbeatStream extends Thread {
    private Subscriber<? super Integer> subscriber;

    private int delay;
    private HeartbeatSensor sensor;

    private Flag flag;

    /**
     * Costruttore
     * @param subscriber subscriber che riceverà i valori emessi
     * @param delay periodo di tempo tra 2 emissioni successive
     * @param flag se settato, termina l'emissione
     */
    public HeartbeatStream(Subscriber subscriber, int delay, Flag flag) {
        this.subscriber = subscriber;
        this.delay = delay;
        this.sensor = new HeartbeatSensor();
        this.flag = flag;
        this.start();
    }

    /**
     * Emette valori fino a che il flag non è settato
     */
    @Override
    public void run() {
        while(!flag.isSet()) {
            subscriber.onNext(sensor.getCurrentValue());
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                subscriber.onError(e);
            }
        }
        subscriber.onCompleted();
    }
}
