package ass08.es01.view;

import ass08.es01.heartbeat.HeartbeatStream;
import ass08.es02.view.utilities.*;
import javafx.util.Pair;
import pap.ass08.pos.Flag;
import pap.ass08.pos.P2d;
import pap.ass08.pos.PosSensor;
import rx.Observable;

import java.util.List;

/**
 * Controller
 * Gestisce il comportamento del programma
 */
public class Controller {
    private Flag flag;
    private Flag started;
    private PosSensor posSensor;

    private Observable<Integer> heartbeatObservable;
    private Observable<P2d> posObservable;

    private List<P2d> pointsToShow;

    private View view;

    private AverageCalculator avgCalc;
    private InstantSpeedCalculator speedCalc;
    private AlarmManager alarmMgr;
    private HighestFreqManager highestFreqMgr;

    /**
     * Costruttore
     */
    public Controller() {
        this.started = new Flag();

        this.posSensor = new PosSensor();
        this.flag = new Flag();

        this.pointsToShow = new LimitedSizeQueue<>(20);

        this.heartbeatObservable = Observable.create(subscriber -> new HeartbeatStream(subscriber, 500, this.flag));
        this.posObservable = this.posSensor.makeObservable(500, this.flag);

        this.view = new View(this);

        this.avgCalc = new AverageCalculator();
        this.speedCalc = new InstantSpeedCalculator();
        this.alarmMgr = new AlarmManager(100, 100);
        this.highestFreqMgr = new HighestFreqManager();
    }

    /**
     * Metodo invocato alla pressione del pulsante Start/Stop
     */
    public void startStopButtonPressed() {
        this.view.setButtonText("Stop");
        if(!this.started.isSet()) {
            this.started.set();

            this.heartbeatObservable.zipWith(this.posObservable, (f, p) -> new Pair<>(f, p)).subscribe(p -> {
                this.view.setFreqValue(p.getKey());
                this.view.setPosValue(p.getValue());

                this.avgCalc.incAndAdd(p.getKey());
                this.view.setAvgValue(avgCalc.getAvg());

                this.speedCalc.setNewPoint(p.getValue());
                this.view.setSpeedValue(speedCalc.getSpeed());

                this.view.setAlarmValue(alarmMgr.analyzeNewValue(p.getKey()));

                this.highestFreqMgr.analyzeNewValue(p.getValue(), p.getKey());

                this.pointsToShow.add(p.getValue());
                this.view.repaintView();
            });
        } else {
            this.stop();
        }
    }

    /**
     * Metodo invocato per ottenere l'elenco dei punti da mostrare
     * @return la lista di punti
     */
    public List<P2d> getPointsToShow() {
        return pointsToShow;
    }

    /**
     * Metodo invocato per ottenere il punto in cui si è registrata la frequenz amassima e la frequenza massima registrata
     * @return una Pair<P2d, Integer> contenente i dati
     */
    public Pair<P2d, Integer> getHighestFreqPoint() {
        return highestFreqMgr.get();
    }

    /**
     * Metodo invocato per terminare l'esecuzione
     */
    public void stop() {
        this.view.disableButton();
        this.flag.set();
    }

    /**
     * Metodo invocato alla pressione del bottone Set, per impostare nuovi threshold e tempi limite
     * @param maxLength lunghezza del tempo limite
     * @param threshold threshold
     */
    public void setTimeAndThreshold(int maxLength, int threshold) {
        this.alarmMgr.setTimeAndThreshold(maxLength, threshold);
    }

    public static void main(String[] args) {
        new Controller();
    }
}
