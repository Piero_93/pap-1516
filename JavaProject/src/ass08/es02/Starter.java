package ass08.es02;

import akka.actor.ActorSystem;
import akka.actor.Props;
import ass08.es02.actors.ControllerActor;

/**
 * Launcher per il sistema di attori
 */
public class Starter {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("Sensors");
        system.actorOf(Props.create(ControllerActor.class));
    }
}
