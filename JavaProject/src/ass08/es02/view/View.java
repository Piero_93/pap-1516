package ass08.es02.view;

import ass08.es02.actors.ControllerActor;
import pap.ass08.pos.P2d;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

/**
 * View
 * Modella l'interfaccia grafica
 */
public class View extends JFrame {

    private JPanel northPanel;

    /*CONTROL PANEL*/
    private JPanel controlPanel;

    private JLabel freq;
    private JTextField freqValue;
    private JLabel pos;
    private JTextField posValue;
    private JButton start;

    /*STATISTICS PANEL*/
    private JPanel statisticsPanel;

    private JLabel avg;
    private JTextField avgValue;
    private JLabel alarm;
    private JTextField alarmValue;
    private JLabel threshold;
    private JSpinner thresholdValue;
    private JLabel time;
    private JButton set;
    private JSpinner timeValue;
    private JLabel speed;
    private JTextField speedValue;

    /*CENTER PANEL*/
    private JPanel centerPanel;

    private ControllerActor controller;
    DecimalFormat d;

    public View(ControllerActor controller) {
        this.controller = controller;
        this.d = new DecimalFormat("#.##");

        this.setSize(new Dimension(800, 750));
        this.setMaximumSize(new Dimension(800, 750));
        this.setMinimumSize(new Dimension(800, 750));
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.northPanel = new JPanel(new BorderLayout());

        /*CONTROL PANEL*/
        this.controlPanel = new JPanel(new FlowLayout());

        this.freq = new JLabel("Frequency: ");
        this.freqValue = new JTextField("--");
        this.pos = new JLabel("Position: ");
        this.posValue = new JTextField("--");
        this.start = new JButton("Start");

        this.controlPanel.setBorder(BorderFactory.createTitledBorder("Current Values"));

        this.freqValue.setPreferredSize(new Dimension(80, 25));
        this.freqValue.setHorizontalAlignment(SwingConstants.CENTER);

        this.posValue.setPreferredSize(new Dimension(150, 25));
        this.posValue.setHorizontalAlignment(SwingConstants.CENTER);

        this.controlPanel.add(freq);
        this.controlPanel.add(freqValue);
        this.controlPanel.add(pos);
        this.controlPanel.add(posValue);
        this.controlPanel.add(start);

        /*STATISTICS PANEL*/
        this.statisticsPanel = new JPanel(new FlowLayout());

        this.avg = new JLabel("Average: ");
        this.avgValue = new JTextField("--");
        this.alarm = new JLabel("Alarm: ");
        this.alarmValue = new JTextField();
        this.threshold = new JLabel("Thresh (bpm): ");
        this.thresholdValue = new JSpinner();
        this.time = new JLabel("Time (sec): ");
        this.timeValue = new JSpinner();
        this.set = new JButton("Set");
        this.speed = new JLabel("Speed: ");
        this.speedValue = new JTextField("--");

        this.statisticsPanel.setBorder(BorderFactory.createTitledBorder("Statistics and Alerts"));

        this.avgValue.setPreferredSize(new Dimension(80, 25));
        this.avgValue.setHorizontalAlignment(SwingConstants.CENTER);

        this.alarmValue.setPreferredSize(new Dimension(30, 30));
        this.alarmValue.setHorizontalAlignment(SwingConstants.CENTER);

        this.thresholdValue.setPreferredSize(new Dimension(60, 25));
        this.thresholdValue.setValue(100);

        this.timeValue.setPreferredSize(new Dimension(60, 25));
        this.timeValue.setValue(1);

        this.speedValue.setPreferredSize(new Dimension(80, 25));
        this.speedValue.setHorizontalAlignment(SwingConstants.CENTER);

        this.statisticsPanel.add(this.avg);
        this.statisticsPanel.add(this.avgValue);
        this.statisticsPanel.add(this.alarm);
        this.statisticsPanel.add(this.alarmValue);
        this.statisticsPanel.add(this.alarmValue);
        this.statisticsPanel.add(this.threshold);
        this.statisticsPanel.add(this.thresholdValue);
        this.statisticsPanel.add(this.time);
        this.statisticsPanel.add(this.timeValue);
        this.statisticsPanel.add(this.set);
        this.statisticsPanel.add(this.speed);
        this.statisticsPanel.add(this.speedValue);

        //CENTER PANEL
        this.centerPanel = new JPanel(){
            @Override
            public void paint(Graphics g) {
                super.paint(g);

                g.setColor(Color.GREEN);
                g.drawLine(400, 0, 400, 600);
                g.drawLine(0, 300, 800, 300);

                g.setColor(Color.BLUE);
                for(int i = 0; i < controller.getPointsToShow().size(); i++) {
                    P2d currentPoint = controller.getPointsToShow().get(i);
                    P2d lastPoint = i > 0 ? controller.getPointsToShow().get(i - 1) : null;
                    g.fillOval((int) currentPoint.getX() - 2 + 400, (int) currentPoint.getY() - 2 + 300, 4, 4);
                    if((lastPoint != null)) {
                        g.drawLine((int) lastPoint.getX() + 400, (int) lastPoint.getY() + 300, (int) currentPoint.getX() + 400, (int) currentPoint.getY() + 300);
                    }
                }

                g.setColor(Color.RED);
                P2d highestFreqPoint = controller.getHighestFreqPoint().getKey();
                if(highestFreqPoint != null) {
                    g.fillOval((int) highestFreqPoint.getX() - 2 + 400, (int) highestFreqPoint.getY() - 2 + 300, 4, 4);
                    g.drawString(controller.getHighestFreqPoint().getValue() + " bpm", (int) highestFreqPoint.getX() + 405, (int) highestFreqPoint.getY() + 300);
                }
            }
        };

        this.northPanel.add(this.controlPanel, BorderLayout.NORTH);
        this.northPanel.add(this.statisticsPanel, BorderLayout.SOUTH);
        this.getContentPane().add(this.centerPanel, BorderLayout.CENTER);
        this.getContentPane().add(this.northPanel, BorderLayout.NORTH);

        this.start.addActionListener((e) -> controller.startStopButtonPressed());

        this.set.addActionListener((e) -> controller.setTimeAndThreshold((int) timeValue.getValue(), (int) thresholdValue.getValue()));

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                System.exit(0);
            }
        });

        this.setVisible(true);
    }

    /**
     * Imposta il testo del bottone Start/Stop
     * @param text testo da impostare
     */
    public void setButtonText(String text) {
        SwingUtilities.invokeLater(() -> this.start.setText(text));
    }

    /**
     * Disabilita il bottone Start/Stop
     */
    public void disableButton() {
        SwingUtilities.invokeLater(() -> this.start.setEnabled(false));
    }

    /**
     * Imposta i valori di frequenza
     * @param freq frequenza
     */
    public void setFreqValue(int freq) {
        SwingUtilities.invokeLater(() -> this.freqValue.setText(freq + "bpm"));
    }

    /**
     * Imposta i valori di posizione
     * @param pos posizione
     */
    public void setPosValue(P2d pos) {
        SwingUtilities.invokeLater(() -> this.posValue.setText("(" + d.format(pos.getX()) + "; " + d.format(pos.getY()) + ")"));
    }

    /**
     * Imposta i valori di frequenza media
     * @param avg frequenza media
     */
    public void setAvgValue(double avg) {
        SwingUtilities.invokeLater(() -> this.avgValue.setText(d.format(avg) + "bpm"));
    }

    /**
     * Imposta l'attivazione o meno dell'allarme
     * @param alarm allarme
     */
    public void setAlarmValue(boolean alarm) {
        SwingUtilities.invokeLater(() -> {
            if(alarm) {
                this.alarmValue.setText("!!!");
                this.alarmValue.setBackground(Color.RED);
            } else {
                this.alarmValue.setText("");
                this.alarmValue.setBackground(Color.GREEN);
            }
        });
    }

    /**
     * Imposta i valori di velocità media
     * @param speed velocità
     */
    public void setSpeedValue(double speed) {
        SwingUtilities.invokeLater(() -> this.speedValue.setText(d.format(speed) + "m/s"));
    }

    /**
     * Esegue l'aggiornamento grafico della view
     */
    public void repaintView() {
        SwingUtilities.invokeLater(this::repaint);
    }
}

