package ass08.es02.view.utilities;

import java.util.ArrayList;

/**
 * LimitedSizeQueue
 * Coda di lunghezza limitata, utilizzata per mostrare su schermo solo gli ultimi N punti del percorso.
 */
public class LimitedSizeQueue<K> extends ArrayList<K> {

    private int maxSize;

    /**
     * Costruttore
     * @param size lunghezza massima della coda
     */
    public LimitedSizeQueue(int size){
        this.maxSize = size;
    }

    /**
     * Aggiunge un nuovo elemento alla fine della coda e taglia gli elementi più vecchi se la coda è diventata troppo lunga
     * @param k
     * @return
     */
    @Override
    public boolean add(K k){
        boolean r = super.add(k);
        if (size() > maxSize){
            removeRange(0, size() - maxSize - 1);
        }
        return r;
    }
}
