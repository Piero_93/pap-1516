package ass08.es02.view.utilities;

import pap.ass08.pos.P2d;

/**
 * InstantSpeedCalculator
 * Calcola la velocità istantanea, calcolata come lunghezza dell'ultimo spostamento/tempo impiegato
 */
public class InstantSpeedCalculator {
    private P2d lastPoint;
    private P2d newPoint;
    private long lastTime;
    private long newTime;

    /**
     * Restituisce la velocità istantanea
     * @return velocità
     */
    public double getSpeed() {
        if(newPoint == null || lastPoint == null) {
            return 0;
        } else {
            return P2d.distance(newPoint, lastPoint) / (newTime - lastTime);
        }
    }

    /**
     * Imposta un nuovo punto del percorso
     * @param newPoint nuovo punto
     */
    public void setNewPoint(P2d newPoint) {
        lastTime = newTime;
        newTime = System.currentTimeMillis();
        lastPoint = this.newPoint;
        this.newPoint = newPoint;
    }
}
