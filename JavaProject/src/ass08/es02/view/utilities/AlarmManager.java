package ass08.es02.view.utilities;

/**
 * AlarmManager
 * Gestione dell'allarme se la frequenza supera un valore di threshold per una durata di tempo impostabile
 */
public class AlarmManager {
    private int threshold;
    private int maxLength;

    private long lastTime;

    /**
     * Costruttore
     * @param maxLength tempo dopo il quale deve entrare in funzione l'allarme
     * @param threshold valore critico di frequenza
     */
    public AlarmManager(int maxLength, int threshold) {
        this.maxLength = maxLength * 1000;
        this.threshold = threshold;
    }

    /**
     * Riceve un nuovo valore e lo analizza, eventualmente attivando l'allarme
     * @param newValue nuovo valore in input
     * @return true se l'allarme è attivo, altrimenti false
     */
    public boolean analyzeNewValue(int newValue) {
        if(newValue > threshold) {
            if(lastTime == 0) {
                this.lastTime = System.currentTimeMillis();
            } else {
                return System.currentTimeMillis() - this.lastTime > this.maxLength;
            }
        } else {
            this.lastTime = 0;
        }
        return false;
    }

    /**
     * Imposta i valori di tempo e threshold
     * @param maxLength tempo (sec)
     * @param threshold threshold (bpm)
     */
    public void setTimeAndThreshold(int maxLength, int threshold) {
        this.maxLength = maxLength * 1000;
        this.threshold = threshold;
        this.lastTime = 0;
    }
}
