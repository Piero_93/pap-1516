package ass08.es02.view.utilities;

import javafx.util.Pair;
import pap.ass08.pos.P2d;

import static java.lang.Math.max;

/**
 * HighestFreqManager
 * Mantiene il punto in cui si è registrata la frequenza massima
 */
public class HighestFreqManager {
    private P2d point;
    private int freq;

    /**
     * Costruttore
     */
    public HighestFreqManager() {
        point = null;
        freq = 0;
    }

    /**
     * Restituisce il punto in cui si è registrata la frequenz amassima e la frequenza massima registrata
     * @return una Pair<P2d, Integer> contenente i dati
     */
    public Pair<P2d, Integer> get() {
        return new Pair<>(point, freq);
    }

    /**
     * Analizza un nuovo valore in input.
     * Se la frequenza registrata è maggiore di quella massima fino ad ora lo memorizza, altrimenti lo scarta.
     * @param point punto
     * @param freq frequenza
     */
    public void analyzeNewValue(P2d point, int freq) {
        this.point = freq >= this.freq ? point : this.point;
        this.freq = max(freq, this.freq);
    }
}
