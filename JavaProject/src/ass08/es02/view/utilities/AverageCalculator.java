package ass08.es02.view.utilities;

/**
 * AverageCalculator
 * Tiene traccia della media dei valori
 */
public class AverageCalculator {
    private int counter = 0;
    private int sum = 0;

    /**
     * Restituisce la media dei valori fino ad ora
     * @return la media
     */
    public double getAvg() {
        return ((double) sum)/((double) counter);
    }

    /**
     * Aggiunge un nuovo valore di cui tenere conto
     * @param newValue nuovo valore da sommare
     */
    public void incAndAdd(int newValue) {
        this.counter++;
        this.sum += newValue;
    }
}
