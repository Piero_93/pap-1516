package ass08.es02.messages;

/**
 * Created by Piero on 07/06/16.
 */
public class TerminationMessage {
    private static TerminationMessage instance;

    private TerminationMessage() {}

    public static TerminationMessage getInstance() {
        if (instance == null) {
            instance = new TerminationMessage();
        }
        return instance;
    }
}
