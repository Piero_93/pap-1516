package ass08.es02.messages;

/**
 * Created by Piero on 07/06/16.
 */
public class ResponseMessage<T> {
    T value;

    public ResponseMessage(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
