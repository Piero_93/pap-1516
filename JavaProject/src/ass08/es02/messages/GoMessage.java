package ass08.es02.messages;

/**
 * Created by Piero on 07/06/16.
 */
public class GoMessage {
    private static GoMessage instance;

    private GoMessage() {}

    public static GoMessage getInstance() {
        if (instance == null) {
            instance = new GoMessage();
        }
        return instance;
    }
}
