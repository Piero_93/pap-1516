package ass08.es02.messages;

import akka.actor.ActorRef;

/**
 * Created by Piero on 07/06/16.
 */
public class ControllerMessage {
    private ActorRef controllerActor;

    public ControllerMessage(ActorRef controllerActor) {
        this.controllerActor = controllerActor;
    }

    public ActorRef getControllerActor() {
        return controllerActor;
    }
}
