package ass08.es02.messages;

/**
 * Created by Piero on 04/07/16.
 */
public class TerminatedMessage {
    private static TerminatedMessage instance;

    private TerminatedMessage() {}

    public static TerminatedMessage getInstance() {
        if (instance == null) {
            instance = new TerminatedMessage();
        }
        return instance;
    }
}
