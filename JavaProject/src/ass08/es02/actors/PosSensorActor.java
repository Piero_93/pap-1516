package ass08.es02.actors;

import akka.actor.PoisonPill;
import akka.actor.UntypedActor;
import ass08.es02.messages.GoMessage;
import ass08.es02.messages.ResponseMessage;
import ass08.es02.messages.TerminationMessage;
import pap.ass08.pos.PosSensor;

/**
 * Modella un attore che gestisce l'estrazione di posizioni dal PosSensor
 */
public class PosSensorActor extends UntypedActor {
    private PosSensor posSensor;

    public PosSensorActor() {
        posSensor = new PosSensor();
    }

    /**
     * Riceve e processa messaggi di vario tipo
     * @param message
     *  - GoMessage: estrae un nuovo valore e lo comunica al controller
     *  - TerminationMessage: termina la sua esecuzione
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof GoMessage) {
            getSender().tell(new ResponseMessage<>(posSensor.getCurrentValue()), this.getSelf());
        } else if(message instanceof TerminationMessage) {
            this.getSelf().tell(PoisonPill.getInstance(), this.getSelf());
        }
    }
}
