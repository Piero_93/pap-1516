package ass08.es02.actors;

import akka.actor.*;
import ass08.es02.messages.*;
import ass08.es02.view.View;
import ass08.es02.view.utilities.*;
import javafx.util.Pair;
import pap.ass08.pos.Flag;
import pap.ass08.pos.P2d;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Modella l'attore controller, che gestisce tutti gli altri e la GUI
 */
public class ControllerActor extends UntypedActor {
    private Flag started; //Vero se è stato premuto "start"
    private Flag stopped; //Vero se l'esecuzione è terminata

    //Riferimenti agli altri attori
    private ActorRef heartbeatSensorActor;
    private ActorRef posSensorActor;

    private List<P2d> pointsToShow;

    private View view;

    private AverageCalculator avgCalc;
    private InstantSpeedCalculator speedCalc;
    private AlarmManager alarmMgr;
    private HighestFreqManager highestFreqMgr;

    //Liste di valori già ricevuti ma non ancora processati
    private Queue<Integer> notProcessedHeartbeatValue;
    private Queue<P2d> notProcessedPosValue;

    /**
     * Costruttore
     */
    public ControllerActor() {
        this.started = new Flag();
        this.stopped = new Flag();

        this.pointsToShow = new LimitedSizeQueue<>(20);

        this.view = new View(this);

        this.avgCalc = new AverageCalculator();
        this.speedCalc = new InstantSpeedCalculator();
        this.alarmMgr = new AlarmManager(100, 100);
        this.highestFreqMgr = new HighestFreqManager();

        this.notProcessedHeartbeatValue = new LinkedBlockingDeque<>();
        this.notProcessedPosValue = new LinkedBlockingQueue<>();
    }

    /**
     * Metodo invocato alla pressione del pulsante Start/Stop
     */
    public void startStopButtonPressed() {
        this.view.setButtonText("Stop");
        if(!this.started.isSet()) {
            this.started.set();
            new Thread() {
                @Override
                public void run() {
                    //Creazione degli attori
                    heartbeatSensorActor = getContext().system().actorOf(Props.create(HeartbeatSensorActor.class));
                    posSensorActor = getContext().system().actorOf(Props.create(PosSensorActor.class));

                    //Watching
                    context().watch(heartbeatSensorActor);
                    context().watch(posSensorActor);

                    //Loop infinito (su thread separato dall'attore) che comunica agli attori di estrarre i valori dai sensori
                    while(!stopped.isSet()) {
                        heartbeatSensorActor.tell(GoMessage.getInstance(), ControllerActor.this.getSelf());
                        posSensorActor.tell(GoMessage.getInstance(), ControllerActor.this.getSelf());
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {}
                    }
                }
            }.start();
        } else {
            this.stop();
        }
    }

    /**
     * Metodo invocato per ottenere l'elenco dei punti da mostrate
     * @return
     */
    public List<P2d> getPointsToShow() {
        return pointsToShow;
    }

    /**
     * Metodo invocato per ottenere il punto in cui si è registrata la frequenz amassima e la frequenza massima registrata
     * @return una Pair<P2d, Integer> contenente i dati
     */
    public Pair<P2d, Integer> getHighestFreqPoint() {
        return highestFreqMgr.get();
    }

    /**
     * Metodo invocato per terminare l'esecuzione
     */
    public void stop() {
        this.view.disableButton();
        this.stopped.set();

        //Comunica agli attori di terminare
        heartbeatSensorActor.tell(TerminationMessage.getInstance(), this.getSelf());
        posSensorActor.tell(TerminationMessage.getInstance(), this.getSelf());
    }

    /**
     * Metodo invocato alla pressione del bottone Set, per impostare nuovi threshold e tempi limite
     * @param maxLength lunghezza del tempo limite
     * @param threshold threshold
     */
    public void setTimeAndThreshold(int maxLength, int threshold) {
        this.alarmMgr.setTimeAndThreshold(maxLength, threshold);
    }

    /**
     * Riceve e processa messaggi di vario tipo
     * @param message
     *  - ResponseMessage: riceve i valori dai due sensori
     *  - Terminated: riceve notifiche dagli attori figli, se sono tutti terminati chiude sé stesso
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof ResponseMessage) {
            //Riconosce il mittente e gestisce il dato in input
            Object response = ((ResponseMessage) message).getValue();
            if(this.getSender() == heartbeatSensorActor) {
                notProcessedHeartbeatValue.add((Integer) response);
            } else if(this.getSender() == posSensorActor) {
                    notProcessedPosValue.add((P2d) response);
            } else {
                throw new IllegalArgumentException();
            }
            this.zipAndUpdate();
        } else if(message instanceof Terminated) {
            //Riconosce il mittente e lo segna come "terminato"
            if(getSender() == heartbeatSensorActor) {
                heartbeatSensorActor = null;
            } else if(getSender() == posSensorActor) {
                posSensorActor = null;
            } else {
                throw new IllegalArgumentException();
            }

            //Se tutti gli attori sono terminati, termina anche l'attore corrente
            if (heartbeatSensorActor == null && posSensorActor == null) {
                getSelf().tell(PoisonPill.getInstance(), getSelf());
            }
        }
    }

    /**
     * Esegue lo zipping degli elementi (unisce i punti e le frequenze corrispondenti) e aggiorna i dati se presenti
     */
    private void zipAndUpdate() {
        if(notProcessedPosValue.peek() != null && notProcessedHeartbeatValue.peek() != null) {
            P2d currentPos = notProcessedPosValue.poll();
            Integer currentFreq = notProcessedHeartbeatValue.poll();

            this.view.setFreqValue(currentFreq);
            this.view.setPosValue(currentPos);

            this.avgCalc.incAndAdd(currentFreq);
            this.view.setAvgValue(avgCalc.getAvg());

            this.speedCalc.setNewPoint(currentPos);
            this.view.setSpeedValue(speedCalc.getSpeed());

            this.view.setAlarmValue(alarmMgr.analyzeNewValue(currentFreq));

            this.highestFreqMgr.analyzeNewValue(currentPos, currentFreq);

            this.pointsToShow.add(currentPos);
            this.view.repaintView();
        }
    }
}
