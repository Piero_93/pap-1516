package ass06.es01.sourcecode;

public class Msg {

	private String msg;
	
	public Msg(String msg){
		this.msg = msg;
	}
	
	public String getContent(){
		return msg;
	}
}
