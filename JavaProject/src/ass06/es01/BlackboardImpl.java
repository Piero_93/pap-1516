package ass06.es01;

import ass06.es01.sourcecode.Blackboard;
import ass06.es01.sourcecode.Msg;

import java.util.*;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.01
 *
 * Implementazione della Blackboard
 */
public class BlackboardImpl implements Blackboard {
    Map<String, Stack<Msg>> board;

    public BlackboardImpl() {
        this.board = new HashMap<>();
    }

    @Override
    public synchronized void post(String tag, Msg msg) {
        board.computeIfAbsent(tag, k -> new Stack<>()).push(msg);
        notifyAll();
    }

    @Override
    public synchronized Msg take(String tag) {
        while((board.getOrDefault(tag, new Stack<>())).size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        return board.get(tag).pop();
    }

    @Override
    public synchronized Optional<Msg> takeIfPresent(String tag) {
        if((board.getOrDefault(tag, new Stack<>())).size() != 0) {
            return Optional.of(board.get(tag).pop());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public synchronized Msg read(String tag) {
        while((board.getOrDefault(tag, new Stack<>())).size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        return board.get(tag).peek();
    }

    @Override
    public synchronized Optional<Msg> readIfPresent(String tag) {
        if((board.getOrDefault(tag, new Stack<>())).size() != 0) {
            return Optional.of(board.get(tag).peek());
        } else {
            return Optional.empty();
        }
    }
}
