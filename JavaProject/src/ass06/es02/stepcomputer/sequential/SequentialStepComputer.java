package ass06.es02.stepcomputer.sequential;

import ass06.es02.Matrix;
import ass06.es02.stepcomputer.StepComputer;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.02
 *
 * Esecutore delle operazioni in maniera sequenziale.
 */
public class SequentialStepComputer extends StepComputer {

    /**
     * Costruttore
     * @param matrix matrice su cui lavorare
     */
    public SequentialStepComputer(Matrix matrix) {
        super.matrix = matrix;
    }

    @Override
    public void performStep() {
        cloneMatrix();

        for(int i = 0; i < matrix.getColumnNumber(); i++) {
            boolean[] column = new boolean[matrix.getRowNumber()];
            for (int j = 0; j < matrix.getRowNumber(); j++) {
                column[j] = auxiliaryMatrix.getNumberOfNeighbors(i, j) == 2;
            }
            matrix.setColumn(i, column);
        }
    }
}
