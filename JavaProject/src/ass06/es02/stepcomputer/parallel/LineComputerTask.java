package ass06.es02.stepcomputer.parallel;

import ass06.es02.Matrix;
import javafx.util.Pair;

import java.util.concurrent.Callable;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.02
 *
 * Task base per l'aggiornamento della matrice di Seeds. Dati un indice di colonna e la matrice, calcola la nuova colonna.
 * La divisione dei task viene fatta per colonne.
 */
public class LineComputerTask implements Callable<Pair<Integer, boolean[]>> {
    private Matrix auxiliaryMatrix;
    private int columnIndex;

    private boolean[] result;

    /**
     * Costruttore
     * @param auxiliaryMatrix matrice da utilizzare per leggere i valori
     * @param columnIndex indice della colonna da aggiornare
     */
    public LineComputerTask(Matrix auxiliaryMatrix, int columnIndex) {
        this.auxiliaryMatrix = auxiliaryMatrix;
        this.columnIndex = columnIndex;

        this.result = new boolean[auxiliaryMatrix.getRowNumber()];
    }

    /**
     * Esegue l'evoluzione di una colonna
     * @return una Pair contenente l'indice della colonna e la colonna evoluta
     * @throws Exception
     */
    @Override
    public Pair<Integer, boolean[]> call() throws Exception {
        for(int i = 0; i < auxiliaryMatrix.getRowNumber(); i++) {
            try {
                result[i] = auxiliaryMatrix.getNumberOfNeighbors(columnIndex, i) == 2;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return new Pair<>(columnIndex, this.result);
    }
}
