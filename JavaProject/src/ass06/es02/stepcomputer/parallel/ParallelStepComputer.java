package ass06.es02.stepcomputer.parallel;

import ass06.es02.Matrix;
import ass06.es02.stepcomputer.StepComputer;
import javafx.util.Pair;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.02
 *
 * Master per la gestione dei task per l'aggiornamento parallelo della matrice.
 * La divisione dei task viene fatta per colonne, infatti:
 *  - Il numero di colonne è generalmente maggiore del numero di core, e comunque sufficiente per fare una divisione
 *      abbastanza fine per avere lo speed up ma non troppo tanto da avere overhead.
 *  - Tutte le colonne hanno lo stesso carico di lavoro, dato esclusivamente dalla lunghezza della colonna stessa.
 */
public class ParallelStepComputer extends StepComputer {
    private ExecutorService executor;

    /**
     * Costruttore
     * @param poolSize numero di thread che eseguiranno il calcolo parallelo
     * @param matrix matrice su cui lavorare
     */
    public ParallelStepComputer(int poolSize, Matrix matrix) {
        this.executor = Executors.newFixedThreadPool(poolSize);
        super.matrix = matrix;
    }

    /**
     * Termina l'esecuzione dell'executor.
     */
    public synchronized void shutdown() {
        executor.shutdown();
    }

    @Override
    public synchronized void performStep() {
        Set<Future<Pair<Integer, boolean[]>>> results = new HashSet<>();

        cloneMatrix();

        for (int i = 0; i < matrix.getColumnNumber(); i++) {
            results.add(executor.submit(new LineComputerTask(auxiliaryMatrix, i)));
        }

        for (Future<Pair<Integer, boolean[]>> result : results) {
            try {
                matrix.setColumn(result.get().getKey(), result.get().getValue());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
