package ass06.es02.stepcomputer;

import ass06.es02.Matrix;

/**
 * Created by Piero on 20/05/16.
 */
public abstract class StepComputer {
    protected Matrix matrix;
    protected Matrix auxiliaryMatrix;

    /**
     * Clona la matrice nella variabile di appoggio. Eseguire questo metodo prima di iniziare le modifiche.
     */
    public void cloneMatrix() {
        auxiliaryMatrix = new Matrix(matrix.getColumnNumber(), matrix.getRowNumber());

        for(int i = 0; i < matrix.getColumnNumber(); i++) {
            boolean[] column = new boolean[matrix.getRowNumber()];
            for (int j = 0; j < matrix.getRowNumber(); j++) {
                column[j] = matrix.getCell(i, j);
            }
            auxiliaryMatrix.setColumn(i, column);
        }
    }

    /**
     * Esegue un singolo passo di aggiornamento
     */
    public abstract void performStep();
}
