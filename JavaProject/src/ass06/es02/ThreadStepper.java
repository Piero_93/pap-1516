package ass06.es02;

import ass06.es02.stepcomputer.parallel.ParallelStepComputer;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.02
 *
 * Thread per l'esecuzione asincrona e ripetuta del ParallelStepComputer.
 */
public class ThreadStepper implements Runnable {
    private ParallelStepComputer executor;
    private int sleepingTime;
    private Status status;
    private ThreadStepperListener listener;

    /**
     * Costruttore
     * @param poolSize numero di thread da utilizzare per la parallelizzazione del calcolo
     * @param matrix matrice da evolvere
     */
    public ThreadStepper(int poolSize, Matrix matrix) {
        executor = new ParallelStepComputer(poolSize, matrix);
        status = Status.READY;

        this.sleepingTime = 0;
    }

    /**
     * Costruttore
     * @param poolSize numero di thread da utilizzare per la parallelizzazione del calcolo
     * @param matrix matrice da evolvere
     * @param sleepingTime tempo di attesa tra l'esecuzione di uno step e l'altro
     * @param listener ascoltatore da notificare al momento del completamento di ogni step
     */
    public ThreadStepper(int poolSize, Matrix matrix, int sleepingTime, ThreadStepperListener listener) {
        this(poolSize, matrix);

        this.sleepingTime = sleepingTime;
        this.listener = listener;
    }

    @Override
    public void run() {
        System.out.println("Started");
        status = Status.RUNNING;
        while(status == Status.RUNNING || status == Status.SUSPENDED) {
            executor.performStep();
            if(listener != null) {
                listener.stepDone();
            }
            try {
                Thread.sleep(sleepingTime);
                checkAndWait();
            } catch (InterruptedException e) {}

        }
        System.out.println("Ended");
    }

    /**
     * Notifica al thread la volontà di terminare l'esecuzione. Avverrà al termine dell'iterazione corrente.
     */
    public synchronized void end() {
        status = Status.STOPPED;
        executor.shutdown();
        notify();
    }

    /**
     * Notifica al thread la volontà di sospendere l'esecuzione. Avverà al termine dell'iterazione corrente.
     * @throws InterruptedException
     */
    public synchronized void suspend() throws InterruptedException {
        if(status == Status.STOPPED) {
            throw new InterruptedException();
        }
        if(status == Status.RUNNING) {
            status = Status.SUSPENDED;
        }
    }

    /**
     * Notifica al thread la volonta di riprendere l'esecuzione.
     * @throws InterruptedException
     */
    public synchronized void resume() throws InterruptedException {
        if(status == Status.STOPPED) {
            throw new InterruptedException();
        }
        if(status == Status.SUSPENDED) {
            status = Status.RUNNING;
            notify();
        }
    }

    /**
     * Controlla lo stato dell'esecutore ed eventualmente si sospende.
     * @throws InterruptedException
     */
    private synchronized void checkAndWait() throws InterruptedException {
        while(status == Status.SUSPENDED) {
            System.out.println("Paused");
            wait();
            System.out.println("Resumed");
        }
    }

    /**
     * Imposta il tempo di attesa tra un'iterazione e l'altra
     * @param sleepingTime tempo di attesa desiderato
     */
    public void setSleepingTime(int sleepingTime) {
        this.sleepingTime = sleepingTime;
    }
}

/**
 * Enum che rappresenta i possibili stati del thread
 */
enum Status {
    READY,
    RUNNING,
    STOPPED,
    SUSPENDED
}
