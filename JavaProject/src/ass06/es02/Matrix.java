package ass06.es02;

import java.util.Random;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.02
 *
 * Modella la matrice, consentendo di contare i vicini di una cella, di sostituire un'intera colonna,
 * di ottenere le dimensioni della matrice e il valore di ogni cella.
 */
 public class Matrix {
    private boolean[][] matrix;
    private int columnNumber;
    private int rowNumber;

    /**
     * Costruttore, inizializza la matrice vuota
     * @param columnNumber larghezza della matrice
     * @param rowNumber altezza della matrice
     */
    public Matrix(int columnNumber, int rowNumber) {
        this.columnNumber = columnNumber;
        this.rowNumber = rowNumber;
        this.matrix = new boolean[columnNumber][rowNumber];
    }

    /**
     * Costruttore, inizializza la matrice con alcune celle valorizzate in modo che si "auto-sostenti" all'infinito
     * @param columnNumber larghezza della matrice
     * @param rowNumber altezza della matrice
     * @param randomSeed numero random
     */
    public Matrix(int columnNumber, int rowNumber, int randomSeed) {
        this(columnNumber, rowNumber);

        /*
         *  Forma inizializzata simile alla seguente:
         *              ..............
         *              .            .
         *              .    * *     .
         *              .    *       .
         *              .            .
         *              ..............
         */

        Random r = new Random(randomSeed);
        int x = 1 + r.nextInt(columnNumber - 2);
        int y = 1 + r.nextInt(rowNumber - 2);


        matrix[x][y] = true;
        matrix[x + 1][y] = true;
        matrix[x][y + 1] = true;
    }

    /**
     * Ottiene il numero di vicini nella configurazione precedente (prima che venisse fatta qualunque modifica)
     * @param x coordinata x della cella di cui si vuole ottenere il numero di vicini
     * @param y coordinata y della cella di cui si vuole ottenere il numero di vicini
     * @return il numero di vicini
     */
    public int getNumberOfNeighbors(int x, int y) {
            if(x >= columnNumber || y >= rowNumber) {
                throw new IllegalArgumentException("Out of bound! (" + x + "," + y + "), max is (" + columnNumber + "," + rowNumber + ")");
            }

        int counter = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j =  y - 1; j <= y + 1; j++) {
                if((i == x && j == y) || j < 0 || i < 0 || j >= rowNumber || i >= columnNumber) {
                    continue;
                }
                counter += matrix[i][j] ? 1 : 0;
            }
        }

        return counter;
    }

    /**
     * Sostituisce colonna della matrice con quella passata come parametro
     * @param index indice della colonna da sostituire
     * @param column colonna da inserire
     */
    public void setColumn(int index, boolean[] column) {
        matrix[index] = column;
    }

    /**
     * Restituisce la larghezza della matrice (numero di colonne)
     * @return la larghezza della matrice
     */
    public int getColumnNumber() {
        return columnNumber;
    }

    /**
     * Restituisce l'altezza della matrice (numero di righe)
     * @return l'altezza della matrice
     */
    public int getRowNumber() {
        return rowNumber;
    }

    /**
     * Ottiene il valore di una cella
     * @param x coordinata x della cella di cui ottenere il valore
     * @param y coordinata y della cella di cui ottenere il valore
     * @return il valore
     */
    public boolean getCell(int x, int y) {
        if(x >= columnNumber || y >= rowNumber) {
            throw new IllegalArgumentException("Out of bound! (" + x + "," + y + "), max is (" + columnNumber + "," + rowNumber + ")");
        }

        return matrix[x][y];
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                s += (matrix[i][j] ? 1 : 0) + "\t";
            }
            s += "\n";
        }
        return s;
    }
}
