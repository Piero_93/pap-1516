package ass06.es02;

import ass06.es02.stepcomputer.parallel.ParallelStepComputer;
import ass06.es02.stepcomputer.sequential.SequentialStepComputer;

/**
 * Biagini Piero - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 06 (per 2016-05-29)
 *
 * Assignment 6.02
 *
 * Tester per effettuare confronti tra l'esecuzione sequenziale e quella parallela
 */
public class TestSequentialVsParallel {
    public static void main(String[] args) {
        final int NUMBER;
        final int X_SIZE;
        final int Y_SIZE;

        if(args.length == 3) {
            try {
                NUMBER = Integer.parseInt(args[0]);
                X_SIZE = Integer.parseInt(args[1]);
                Y_SIZE = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                System.err.println("Incorrect parameters. Please insert 3 integer numbers or nothing.");
                return;
            }

        } else {
            NUMBER = 1000;
            X_SIZE = 2000;
            Y_SIZE = 2000;
        }
        System.out.println("Starting with " + NUMBER + " iterations, table size is " + X_SIZE + "x" + Y_SIZE + ".\n");



        //Esecuzione sequenziale
        System.out.println("Starting sequential. (Expected time with default parameters: ~ 120 sec)");
        long start = System.currentTimeMillis();
        SequentialStepComputer seqComputer = new SequentialStepComputer(new Matrix(X_SIZE, Y_SIZE, 5));
        for(int i = 0; i < NUMBER; i++) {
            seqComputer.performStep();
        }
        System.out.println("Sequential time = " + (System.currentTimeMillis() - start));
        //System.out.println(seqComputer.getMatrix());

        System.out.println("-----");

        System.out.println("Starting parallel. (Expected time with default parameters: ~ 30 sec)");
        start = System.currentTimeMillis();
        ParallelStepComputer parComputer = new ParallelStepComputer(Runtime.getRuntime().availableProcessors(), new Matrix(X_SIZE, Y_SIZE, 5));
        for(int i = 0; i < NUMBER; i++) {
            parComputer.performStep();
        }
        parComputer.shutdown();
        System.out.println("Parallel time = " + (System.currentTimeMillis() - start));
        //System.out.println(parComputer.getMatrix());
    }
}
