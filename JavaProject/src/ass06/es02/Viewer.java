package ass06.es02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;

/**
 * Modella la GUI per la visualizzazione dell'automa.
 */
public class Viewer extends JFrame implements ThreadStepperListener {
    private Matrix matrix;
    private ThreadStepper executor;

    private final int CONTROL_PANEL_HEIGHT = 100;
    private final int MIN_WIDTH = 500;
    private final int PADDING = 10;
    private static final int MIN_SCALE = 1;

    private final int DEFAULT_SLEEPING_TIME = 500;

    /**
     * Costruttore. Crea la GUI con dimensione prefissata.
     * @param xMatrixSize larghezza della matrice
     * @param yMatrixSize altezza della matrice
     */
    public Viewer(int xMatrixSize, int yMatrixSize) {
        initialize(xMatrixSize, yMatrixSize);
    }

    private void showError() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        JOptionPane.showMessageDialog(this, "Only integer number greater than 0 and less than max values are allowed!\n" +
                "Max width: " + (gd.getDisplayMode().getWidth() - 2 * PADDING) + "\n" +
                "Max height: " + (gd.getDisplayMode().getHeight() - CONTROL_PANEL_HEIGHT - 2 * PADDING - 50), "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Costruttore. Crea la GUI.
     */
    public Viewer() {
        while(matrix == null) {
            new JOptionPane() {
                public void showMessageDialog() {
                    GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
                    int width = gd.getDisplayMode().getWidth();
                    int height = gd.getDisplayMode().getHeight();

                    JTextField xField = new JTextField(5);
                    JTextField yField = new JTextField(5);

                    JPanel panel = new JPanel(new BorderLayout());
                    JPanel controlPanel = new JPanel(new FlowLayout());
                    panel.add(new JLabel("<html>Please enter desired width and height values.<br><b>Only integer number inside specified range.</b><br>" +
                            "&emsp;- Width range: 0 - " + (gd.getDisplayMode().getWidth() - 2 * PADDING) + "<br>" +
                            "&emsp;- Height range: 0 - " + (gd.getDisplayMode().getHeight() - CONTROL_PANEL_HEIGHT - 2 * PADDING - 50) + "</html>"), BorderLayout.CENTER);
                    panel.add(controlPanel, BorderLayout.SOUTH);
                    controlPanel.add(new JLabel("Width:"));
                    controlPanel.add(xField);
                    controlPanel.add(Box.createHorizontalStrut(15)); //Spazio vuoto
                    controlPanel.add(new JLabel("Height:"));
                    controlPanel.add(yField);

                    int result = JOptionPane.showConfirmDialog(null, panel,
                            "Seeds - Specify size", JOptionPane.OK_CANCEL_OPTION);
                    if (result == JOptionPane.OK_OPTION) {
                        try {
                            int insertedX = Integer.parseInt(xField.getText());
                            int insertedY = Integer.parseInt(yField.getText());

                            initialize(insertedX, insertedY);
                        } catch (NumberFormatException e) {
                            showError();
                        }
                    } else {
                        System.exit(0);
                    }
                }
            }.showMessageDialog();
        }

    }

    private void initialize(int xMatrixSize, int yMatrixSize) {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        if(xMatrixSize <= 0 || //La x inserita è troppo piccola
                yMatrixSize <= 0 || //La y inserita è troppo piccola
                xMatrixSize * MIN_SCALE > gd.getDisplayMode().getWidth() - 2 * PADDING || //La x inserita è troppo grande
                yMatrixSize * MIN_SCALE > gd.getDisplayMode().getWidth() - CONTROL_PANEL_HEIGHT - 2 * PADDING - 50) { //La y inserita è troppo grande
            showError();
            return;
        }

        //Impostazioni del frame
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Seed - A Parallel Cellular Automaton");
        this.setLayout(new BorderLayout());
        this.setMinimumSize(new Dimension(
                Math.max(MIN_WIDTH, xMatrixSize + 2 * PADDING),
                Math.max(MIN_WIDTH, yMatrixSize + 2 * PADDING + CONTROL_PANEL_HEIGHT))
        ); //Dimensione minima per contenere l'intera matrixe o tutti i controlli
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if(executor != null) {
                    System.out.println("Shutting down executor...");
                    executor.end();
                }
            }
        });

        //Crea la matrice
        matrix = new Matrix(xMatrixSize, yMatrixSize, new Random().nextInt(xMatrixSize));

        //Dichiarazioni degli oggetti della GUI
        JPanel controlPanel = new JPanel();
        JButton startButton = new JButton("Start");
        JButton pauseButton = new JButton("Pause");
        JSpinner delayTime = new JSpinner();
        JLabel delayTimeLabel = new JLabel("Delay time:");
        JButton setButton = new JButton("Set");
        JButton resetButton = new JButton("Reset");
        JPanel viewPanel = new JPanel(){
            @Override
            public void paint(Graphics g) {
                super.paint(g);

                //Ridimensionamento della matrice
                int cellXSize = (int) Math.floor((double) (this.getWidth() - 2 * PADDING) / (double) xMatrixSize);
                int cellYSize = (int) Math.floor((double) (this.getHeight() - 2 * PADDING) / (double) yMatrixSize);

                //Centratura della matrice
                int fillXBorder = (this.getWidth() - (xMatrixSize * cellXSize) - (2 * PADDING))/2;
                int fillYBorder = (this.getHeight() - (yMatrixSize * cellYSize) - (2 * PADDING))/2;

                //Contorno rosso
                g.setColor(Color.RED);
                g.drawRect(
                        fillXBorder + PADDING - 1,
                        fillYBorder + PADDING - 1,
                        matrix.getColumnNumber() * cellXSize + 1,
                        matrix.getRowNumber() * cellYSize + 1
                );



                //Celle
                int liveCells = 0;
                g.setColor(Color.CYAN);
                for (int i = 0; i < matrix.getColumnNumber(); i++) {
                    for (int j = 0; j < matrix.getRowNumber(); j++) {
                        if(matrix.getCell(i,j)) {
                            liveCells++;
                            g.fillRect(
                                    fillXBorder + PADDING + i * cellXSize,
                                    fillYBorder + PADDING + j * cellYSize,
                                    cellXSize,
                                    cellYSize
                            );
                        }
                    }
                }

                //Scala e altre info
                g.setColor(Color.RED);
                g.setFont(new Font("Arial", Font.BOLD, 10));
                g.drawString("Cell size: " + cellXSize + "x" + cellYSize + " pixels; " +
                                "Matrix size: " + xMatrixSize + "x" + yMatrixSize + ";\tLive cells: " +
                                String.format("%0" + ((int)Math.floor(Math.log10(xMatrixSize * yMatrixSize) + 1)) + "d", liveCells) +
                                "/" + (xMatrixSize * yMatrixSize),
                        PADDING, PADDING - 2);
            }
        };

        //controlPanel
        controlPanel.setSize(new Dimension(getWidth(), CONTROL_PANEL_HEIGHT));

        //startButton
        startButton.addActionListener((event) -> {
            if(executor == null) {
                executor = new ThreadStepper(Runtime.getRuntime().availableProcessors(), matrix, (Integer) delayTime.getValue(), this);
                new Thread(executor).start();
            } else {
                try {
                    executor.resume();
                } catch (InterruptedException e) {}
            }
        });
        controlPanel.add(startButton);

        //pauseButton
        pauseButton.addActionListener((event) -> {
            if(executor != null) {
                try {
                    executor.suspend();
                } catch (InterruptedException e) {}
            }
        });
        controlPanel.add(pauseButton);

        controlPanel.add(Box.createHorizontalStrut(15)); //Spazio vuoto

        //delayTimeLabel
        controlPanel.add(delayTimeLabel);

        //delayTimeNUD
        delayTime.setValue(DEFAULT_SLEEPING_TIME);
        delayTime.setPreferredSize(new Dimension(80, 25));
        delayTime.addMouseWheelListener((e) -> {
            if((Integer) delayTime.getValue() < 0) {
                delayTime.setValue(0);
            }
            delayTime.setValue((Integer) delayTime.getValue() + Integer.signum(e.getUnitsToScroll()) * 10);
            if(executor != null) {
                executor.setSleepingTime((Integer) delayTime.getValue());
            }
        });
        controlPanel.add(delayTime);

        //setButton
        setButton.addActionListener((event) -> {
            if((Integer) delayTime.getValue() < 0) {
                delayTime.setValue(0);
            }
            delayTime.setValue(delayTime.getValue());
            if(executor != null) {
                executor.setSleepingTime((Integer) delayTime.getValue());
            }
        });
        controlPanel.add(setButton);

        controlPanel.add(Box.createHorizontalStrut(15)); //Spazio vuoto

        //resetButton
        resetButton.addActionListener((event) -> {
            if(executor != null) {
                executor.end();
                executor = null;
            }
            matrix = new Matrix(xMatrixSize, yMatrixSize, new Random().nextInt(xMatrixSize));
            repaint();
        });
        controlPanel.add(resetButton);

        //viewPanel
        //viewPanel.setSize(new Dimension(WINDOW_X_SIZE, WIEW_PANEL_HEIGHT));

        //frame
        add(controlPanel, BorderLayout.NORTH);
        add(viewPanel, BorderLayout.CENTER);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                repaint();
            }
        });
        setVisible(true);
    }

    @Override
    public void stepDone() {
        SwingUtilities.invokeLater(this::repaint);
    }

    public static void main(String[] args) {
        if(args.length == 2) {
            try {
                int x = Integer.parseInt(args[0]);
                int y = Integer.parseInt(args[1]);
                new Viewer(x, y);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Incorrect parameters. Please insert 2 integer numbers or nothing.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        } else {
            new Viewer();
        }
    }
}