package ass06.es02;

/**
 * Listener per ThreadStepper, notifica l'ascoltatore ogni volta che la computazione di un passo termina
 */
public interface ThreadStepperListener {
    /**
     * Metodo da chiamare quando la computazione di un passo è terminata
     */
    void stepDone();
}
