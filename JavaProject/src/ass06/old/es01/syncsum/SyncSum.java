/**
 * PAP 2015-2016 - Assignment 06 - Es. 1 - SynchMonitor
 */
package ass06.old.es01.syncsum;

import ass06.old.es01.MySyncAdder;

public class SyncSum {
	public static void main(String[] args) {
		SyncAdder adder = new MySyncAdder();

		new DataProducerA(adder).start();
		new DataProducerB(adder).start();
		new DataConsumer(adder).start();
	}

}
