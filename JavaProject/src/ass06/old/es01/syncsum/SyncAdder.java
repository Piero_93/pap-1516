package ass06.old.es01.syncsum;

public interface SyncAdder {

	void setDataA(int value);
	
	void setDataB(int value);
	
	int getSum();
}
