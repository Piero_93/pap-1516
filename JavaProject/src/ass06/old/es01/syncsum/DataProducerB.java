package ass06.old.es01.syncsum;

import java.util.stream.IntStream;

public class DataProducerB extends Thread {

	private SyncAdder adder;
	private IntStream data;
	
	public DataProducerB(SyncAdder adder){
		this.adder = adder;
		data = IntStream.rangeClosed(1,10).map((value) -> value + 1);
	}
	
	public void run(){
		data.forEach((value) -> {
			String className = this.getClass().getName();
			System.out.println(className.substring(className.lastIndexOf(".") + 1) + " set data: " + value);
			adder.setDataB(value);
		});
	}
	
}
