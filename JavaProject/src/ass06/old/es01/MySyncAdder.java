package ass06.old.es01;


import ass06.old.es01.syncsum.SyncAdder;

/**
 * Implementare il monitor SyncSumImpl (che implementa l’interfaccia SyncSum) in modo che abbia il comportamento di sincronizzazione per cui:
 *      - getSum si blocca se non sono disponibili entrambi i valori delle due sequenze
 *      - setDataA e setDataB si bloccano fin quando la somma corrispondente non viene prelevata.
 *
 * E' un monitor, quindi tutti i metodi pubblici sono synchronized.
 */
public class MySyncAdder implements SyncAdder {
    private int a;
    private int b;

    private boolean aAvailable;
    private boolean bAvailable;
    private boolean sumRead;

    public MySyncAdder() {
        aAvailable = false;
        bAvailable = false;
    }

    @Override
    public synchronized void setDataA(int value) {
        while(aAvailable) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        a = value;
        aAvailable = true;
        if(bAvailable) {
            notifyAll();
        }
    }

    @Override
    public synchronized void setDataB(int value) {
        while(bAvailable) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        b = value;
        bAvailable = true;
        if(aAvailable) {
            notifyAll();
        }
    }

    @Override
    public synchronized int getSum() {
        while(!(aAvailable && bAvailable)) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        aAvailable = false;
        bAvailable = false;
        notifyAll();
        return a + b;
    }
}
