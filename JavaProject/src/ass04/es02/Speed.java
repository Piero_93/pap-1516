package ass04.es02;

import java.util.Random;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.02
 *
 * Rappresenta la velocità date le componenti orizzontali e verticali
 */
public class Speed {
    private int speedX;
    private int speedY;

    /**
     * Costruisce un nuovo oggetto generando la velocità con valori casuali tra 0 e 1
     */
    public Speed() {
        Random r = new Random(System.nanoTime());
        do {
            //Genera un numero tra -5 e 5 e ne estrapola il segno (+1 se maggiore di 0, -1 se minore, 0 se uguale).
            //Metodo utilizzato al posto di estrarre direttamente un numero tra -1 e 1 per diminuire la probabilità
            //che una delle componenti della velocità sia pari a 0 (quindi che la parola vada solo in verticale o orizzontale)
            speedX = (int) Math.signum(r.nextInt(11) - 5);
            speedY = (int) Math.signum(r.nextInt(11) - 5);

            //Nel caso in cui entrambe le componenti valgono 0 (parola ferma), viene rieseguita l'assegnazione della velocità
        } while (speedX == 0 && speedY == 0);
    }

    public Speed(int speedX, int speedY) {
        this.speedX = speedX;
        this.speedY = speedY;
    }

    /**
     *
     */
    public void invertX() {
        speedX = -speedX;
    }

    public void invertY() {
        speedY = - speedY;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getSpeedY() {
        return speedY;
    }
}
