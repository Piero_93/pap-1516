package ass04.es02;

import ass04.es02.textliblibrary.TextLib;
import ass04.es02.textliblibrary.TextLibFactory;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.02
 *
 * Modella una parola che si muove in un box rimbalzando ai lato
 */
public class WordThread extends Thread {
    //Sleeping time massimi e minimi di ogni parola
    private static final int MAX_SLEEPING_TIME = 500;
    private static final int MIN_SLEEPING_TIME = 50;

    //Box in cui le parole si muovono
    private static int top;
    private static int left;
    private static int right;
    private static int bottom;

    //Caratteristiche della parola
    private final String word;
    private final String erasingWord;
    private final TextLib.Color color;
    private final long sleepingTime;
    private final Position lastPos;
    private final Speed speed;

    //Dati per la stampa dei cambiamenti di stato
    private final int statusChangingStartXIndex;
    private final int statusChangingStartYIndex;
    private int statusChangingIndex = 0;
    private final static int MAX_STATUS_CHANGING_SIZE = 2;
    private final static int HEADER_SIZE = 1;

    //Abilitazione o disabilitazione della stampa di debug
    private static boolean debugEnabled = false;

    /**
     * Costruisce un nuovo thread
     * @param top limite superiore
     * @param left limite sinistro
     * @param right limite destro
     * @param bottom limite inferiore
     * @param word contenuto della parola
     * @param statusChangingStartYIndex indice a cui scrivere i cambiamenti di stato
     * @param debugEnabled true se il thread deve stampare le stampe di debug, altrimenti false
     */
    public WordThread(int top, int right, int bottom, int left, String word, int statusChangingStartYIndex, boolean debugEnabled) throws TooLongStringException {
        Random r = new Random(System.nanoTime());

        WordThread.top = top;
        WordThread.left = left;
        WordThread.right = right;
        WordThread.bottom = bottom;
        this.word = word;
        this.erasingWord = getErasingString(word);
        this.statusChangingStartXIndex = right + 4;
        this.statusChangingStartYIndex = statusChangingStartYIndex;

        WordThread.debugEnabled = debugEnabled;

        //Posizione iniziale casuale
        if(right - left - 2 <= word.length()) {
            TextLibFactory.getInstance().writeAt(
                    statusChangingStartXIndex,
                    statusChangingStartYIndex,
                    "\"" + word +"\"" + " is too long for this box so it is not represented!",
                    TextLib.Color.RED
            );
            throw new TooLongStringException();
        }

        lastPos = new Position(r.nextInt(right - left - word.length() - 2) + left + 1, r.nextInt(bottom - top - 2) + top + 1);

        //Direzione iniziale causale
        speed = new Speed();

        //Colore casuale
        color = TextLib.Color.values()[r.nextInt(TextLib.Color.values().length - 2) + 1]; //Avoid Black and White

        //Tempo di attesa tra un movimento e l'altro casuale
        sleepingTime = r.nextInt(MAX_SLEEPING_TIME - MIN_SLEEPING_TIME) + MIN_SLEEPING_TIME;

        TextLibFactory.getInstance().writeAt(
                statusChangingStartXIndex,
                statusChangingStartYIndex,
                color.toString() + " \"" + word +"\"," + " Sleeping time: " + sleepingTime,
                color
        );
    }

    /**
     * Corpo del thread
     */
    @Override
    public void run() {
        while(true) {
            move();
            try {
                sleep(sleepingTime);
            } catch (InterruptedException e) {}
        }
    }

    /**
     * Esegue il movimento della parola
     */
    private void move() {
        //Cancellazione della parola
        TextLibFactory.getInstance().writeAt(lastPos.getX(), lastPos.getY(), erasingWord);

        //Gestione del rimbalzo sui limiti sinistro e destro
        if(lastPos.getX() <= left || lastPos.getX() >= right - word.length() + 1) {
            speed.invertX();

            //Gestione delle stampe di debug
            if(debugEnabled) {
                statusChangingIndex = (statusChangingIndex) % (MAX_STATUS_CHANGING_SIZE - HEADER_SIZE) + 1;
                TextLibFactory.getInstance().writeAt(statusChangingStartXIndex, statusChangingStartYIndex + statusChangingIndex + HEADER_SIZE, "Direction X changed at " + lastPos.toString(), color);
            }
        }

        //Gestione del rimbalzo sui limiti superiore e inferiore
        if(lastPos.getY() <= top || lastPos.getY() >= bottom ) {
            speed.invertY();

            //Gestione delle stampe di debug
            if(debugEnabled) {
                statusChangingIndex = (statusChangingIndex) % (MAX_STATUS_CHANGING_SIZE - HEADER_SIZE) + 1;
                TextLibFactory.getInstance().writeAt(statusChangingStartXIndex, statusChangingStartYIndex + statusChangingIndex + HEADER_SIZE, "Direction Y changed at " + lastPos.toString(), color);
            }
        }

        lastPos.sumX(speed.getSpeedX());
        lastPos.sumY(speed.getSpeedY());

        //Rendering della parola nella nuova posizione
        TextLibFactory.getInstance().writeAt(lastPos.getX(), lastPos.getY(), word, color);

        if(debugEnabled) {
            TextLibFactory.getInstance().writeAt(statusChangingStartXIndex, statusChangingStartYIndex + 1, lastPos.toString() + ", Speed ", color);
        }
    }

    /**
     * Genera una stringa di spazi lunga quanto la parola
     * @return una stringa di spazi lunga quanto la parola
     */
    private static String getErasingString(String word) {
        return IntStream.range(0, word.length()).mapToObj(i -> " ").reduce("", (x, y) -> x + y);
    }

    /**
     * Launcher del programma
     * Utilizzo:
     *  - java ass04.es02.WordThread: lancia il programma senza stampe di debug con le parole di esempio
     *  - java ass04.es02.WordThread <bool>: lancia il programma con o senza stampe di debug (a seconda del valore booleano)
     *          con le parole di esempio
     *  - java ass04.es02.WordThread <bool> <stringhe>: lancia il programma con o senza stampe di debug (a seconda del valore booleano)
     *          con le parole elencate come argomento
     * @param args true se si vogliono visualizzare le stampe di debug
     */
    public static void main(String[] args) {
        //Bordi del box
        final int LEFT = 1;
        final int RIGHT = 80;
        final int TOP = 1;
        final int BOTTOM = 40;

        //Rappresentazione del box su console
        TextLibFactory.getInstance().cls();

        TextLibFactory.getInstance().writeAt(
                LEFT, TOP, IntStream.rangeClosed(LEFT, RIGHT)
                .mapToObj(i -> "*")
                .reduce("", (x,y) -> x + y),
                TextLib.Color.RED
        );
        TextLibFactory.getInstance().writeAt(
                LEFT, BOTTOM, IntStream.rangeClosed(LEFT, RIGHT)
                        .mapToObj(i -> "*")
                        .reduce("", (x,y) -> x + y),
                TextLib.Color.RED
        );

        IntStream.rangeClosed(TOP, BOTTOM).forEach(i -> {
            TextLibFactory.getInstance().writeAt(LEFT, i, "*", TextLib.Color.RED);
            TextLibFactory.getInstance().writeAt(RIGHT, i, "*", TextLib.Color.RED);
        });

        String[] words;
        if(args.length > 1) {
            words = new String[args.length - 1];
            Arrays.stream(args)
                    .skip(1) //Il primo parametro è "true" o "false" per le stampe di debug
                    .collect(Collectors.toList()).toArray(words);
        } else {
            //Vettore di parole di esempio
            words = new String[]{"Hello", "Concurrent", "World", "PAP-1516", "Assignment 4", "Es 2"};
        }

            //Inizializzazione dei thread
            IntStream.range(0, words.length)
                    .forEach(i -> {
                        try {
                            new WordThread(TOP + 1, RIGHT - 1, BOTTOM - 1, LEFT + 1, words[i],
                                    1 + i * (MAX_STATUS_CHANGING_SIZE + 2), args.length > 0 && Boolean.parseBoolean(args[0])).start();
                        } catch (TooLongStringException e) {}
                    });

    }

    private class TooLongStringException extends Exception {}
}