package ass04.es02;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.02
 *
 * Modella un punto della console
 */
public class Position {
    private int x;
    private int y;

    /**
     * Costruttore
     * @param x coordinata x
     * @param y coordinata y
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Ottiene il valore di x
     * @return il valore di x
     */
    public int getX() {
        return x;
    }

    /**
     * Ottiene il valore di y
     * @return il valore di y
     */
    public int getY() {
        return y;
    }

    /**
     * Somma un valore alla coordinata x
     * @param x valore da sommare
     */
    public void sumX(int x) {
        this.x += x;
    }

    /**
     * Somma un valore alla coordinata y
     * @param y valore da sommare
     */
    public void sumY(int y) {
        this.y += y;
    }

    @Override
    public String toString() {
        return "Position (x=" + x + ", y=" + y + ")";
    }
}

