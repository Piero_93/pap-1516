package ass04.es01;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.01
 *
 * Launcher dell'attacco
 */
public class TestSecureSystem {
    /**
     * Lunghezza della password da scoprire.
     * Tempi di massima:
     *  - 3 caratteri: 0.1-0.3 sec
     *  - 4 caratteri: 0.6-1.0 sec
     *  - 5 caratteri: 20-100 sec
     *
     *  Test eseguiti su MacBook Pro con processore Intel Core i7-3615QM @ 2.30GHz (4 core + Hyper Threading)
     *  con 8 thread in esecuzione.
     */


	private final static int PASSWORD_LENGTH = 4;

	public static void main(String[] args) {
		//Avvia l'attacco brute force utilizzando un numero di thread pari al numero di CPU fisiche e virtuali
		new BruteForceExecutor(Runtime.getRuntime().availableProcessors(), PASSWORD_LENGTH, new SecureSystem(PASSWORD_LENGTH));

	}

}
