package ass04.es01;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.01
 *
 * Thread che esegue parte dell'attacco
 *
 * NOTA: System.err è stato utilizzato per mettere in evidenza alcuni messaggi in console
 */
public class BruteForceThread extends Thread {
    private int threadNumber;
    private SecureSystem system;
    private BruteForceExecutor listener;
    private PasswordGenerator generator;
    private long remainingPassword;

    /**
     * Inizializza il thread.
     * Le password sono da considerarsi ordinate in ordine crescente, in base al valore che i singoli byte assumono.
     * Vengono divise staticamente tra i thread in base al numero di thread desiderati e al numero di password totali.
     * @param threadNumber numero d'ordine del thread
     * @param system sistema da bucare
     * @param listener listener da avvisare dell'avvenuta computazione con successo
     * @param passwordLength lunghezza della password
     * @param numberOfPassword numero di password da testare
     * @param alreadyAssignedPassword indice della prima password da testare (cfr. considerazioni sull'ordinamento)
     */
    public BruteForceThread(int threadNumber, SecureSystem system, BruteForceExecutor listener, int passwordLength, long numberOfPassword, long alreadyAssignedPassword) {
        this.threadNumber = threadNumber;
        this.system = system;
        this.listener = listener;
        this.remainingPassword = numberOfPassword;

        this.generator = new PasswordGenerator(passwordLength, alreadyAssignedPassword);
    }

    /**
     * Costringe il thread a terminare prima che siano state testate tutte le password, per terminare il programma
     * non appena uno dei thread ha terminato con successo l'operazione.
     * Se nessun thread termina con successo, il programma viene chiuso automaticamente quando tutte le password
     * sono state provate.
     */
    public void terminate() {
        remainingPassword = 0;
    }

    /**
     * Corpo del thread.
     * Richiede una nuova password al suo generatore e la prova. In caso di successo, notifica il listener.
     * Può terminare:
     *  - Dopo aver trovato la password corretta
     *  - Dopo aver provato tutte le password fornite dal generatore
     *  - In caso un altro thread abbia trovato la password corretta e sia stato chiamato il metodo "terminate"
     */
    @Override
    public void run() {
        System.out.println("Thread " + threadNumber + " spawned!");
        for(; remainingPassword >= 0; remainingPassword--) {
            if(generator.hasNext()) {
                String current = generator.next();
                if (system.login(current)) {
                    System.err.println("Thread " + threadNumber + " found the password!");
                    listener.computationEnded(current);
                    break;
                }
            }
        }

        System.out.println("Thread " + threadNumber + " ended!");
    }
}
