package ass04.es01;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.01
 *
 * Esecutore dell'attacco
 */
public class BruteForceExecutor implements BruteForceThreadListener {
    //Limiti inferiore e superiore del set di caratteri ASCII da utilizzare
    public final static byte CHARACTER_LOWER_BOUND = 32;
    public final static byte CHARACTER_UPPER_BOUND = 127;

    private long millisStart;
    private List<BruteForceThread> threads;

    /**
     * Crea un esecutore dell'attacco
     * NOTA: System.err è stato utilizzato per mettere in evidenza alcuni messaggi in console
     * @param numberOfThreads numero di thread da lanciare
     * @param passwordLength lunghezza della password
     * @param system sistema sicuro da bucare
     */
    public BruteForceExecutor(int numberOfThreads, int passwordLength, SecureSystem system) {
        System.err.println("Starting with " + numberOfThreads + " thread, password is " + passwordLength + " character long.");

        millisStart = System.currentTimeMillis();

        long passwordNumber = (long) Math.pow(CHARACTER_UPPER_BOUND - CHARACTER_LOWER_BOUND + 1, passwordLength);
        long passwordPerThread = passwordNumber / numberOfThreads;

        //Crea i thread registrandosi come listener, li salva in una lista e li avvia
        threads = new ArrayList<>();

        threads = IntStream.range(0, numberOfThreads -1)
                .mapToObj(i -> new BruteForceThread(i + 1, system, this, passwordLength, passwordPerThread, passwordPerThread * i))
                .collect(Collectors.toList());
        threads.forEach(BruteForceThread::start);

        threads.add(new BruteForceThread(numberOfThreads, system, this, passwordLength, passwordNumber - passwordPerThread * (numberOfThreads - 1), passwordPerThread * numberOfThreads));
        threads.get(numberOfThreads - 1).start();
    }

    @Override
    public void computationEnded(String correctPassword) {
        System.err.println("Computation ended. Elapsed time: " + (System.currentTimeMillis() - millisStart)/1000.0 + " seconds. \n" +
                "The password is \"" + correctPassword + "\".");
        threads.stream().forEach(BruteForceThread::terminate);
    }
}
