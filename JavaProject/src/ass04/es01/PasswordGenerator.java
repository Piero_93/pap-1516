package ass04.es01;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.IntStream;

import static ass04.es01.BruteForceExecutor.CHARACTER_LOWER_BOUND;
import static ass04.es01.BruteForceExecutor.CHARACTER_UPPER_BOUND;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.01
 *
 * Generatore sequenziale di password.
 * Le password sono da considerarsi ordinate in ordine crescente, in base al valore che i singoli byte assumono.
 * Vengono divise staticamente tra i thread in base al numero di thread desiderati e al numero di password totali.
 */
public class PasswordGenerator implements Iterator<String> {
    private final byte[] currentPassword;

    /**
     * Crea il generatore di password
     * @param passwordLength lunghezza della password da ricercare
     * @param alreadyTestedPassword indice della prima password da generare (cfr. considerazioni sull'ordinamento)
     * @throws IllegalArgumentException se la lunghezza della password, il numero di password assumono valori minori o
     *      uguali a 0 o l'indice della prima password assume valore negativo
     */
    public PasswordGenerator(int passwordLength, long alreadyTestedPassword) throws IllegalArgumentException {
        if(passwordLength == 0 ||  alreadyTestedPassword < 0) {
            throw new IllegalArgumentException();
        }
        this.currentPassword = new byte[passwordLength];

        for(int i = passwordLength - 1; i >= 0; i--) {
            currentPassword[i] = (byte) (CHARACTER_LOWER_BOUND + (alreadyTestedPassword % (CHARACTER_UPPER_BOUND - CHARACTER_LOWER_BOUND + 1)));
            alreadyTestedPassword = alreadyTestedPassword / (CHARACTER_UPPER_BOUND - CHARACTER_LOWER_BOUND + 1);
        }
    }

    /**
     * Verifica se esiste una password successiva
     * @return true se esiste una password successiva a questa
     */
    @Override
    public boolean hasNext() {
        int accumulator = 0;
        for (byte b : currentPassword) {
            accumulator += b;
        }
        //Nota: impossibile utilizzare la programmazione funzionale in quanto byte si comporta differentemente da Byte.

        //Esiste il next solo se la password ha un valore compreso tra il minimo e il massimo.
        //Posso fare questo confronto perchè tutti vengono inizializzati a LOWER_BOUND e il valore di ognuno non supera mai UPPER_BOUND.
        //L'unico modo per passare oltre a UPPER_BOUND * lunghezza o LOWER_BOUND * lunghezza è che il valore di qualche byte non sia valido.
        return accumulator <= CHARACTER_UPPER_BOUND * currentPassword.length &&
                accumulator >= CHARACTER_LOWER_BOUND * currentPassword.length;
    }

    /**
     * Restituisce la password corrente e calcola quella successiva, se esiste
     * @return la password corrente
     */
    @Override
    public String next() {
        String password = new String(currentPassword);

        for (int i = currentPassword.length - 1; i >= 0; i--) {
            if (currentPassword[i] < CHARACTER_UPPER_BOUND) {
                currentPassword[i]++;
                return password;
            } else {
                currentPassword[i] = CHARACTER_LOWER_BOUND;
            }
        }

        for (int i = 0; i < currentPassword.length; i++) {
            currentPassword[i] = (byte) 0; //Immetto un valore non valido perché ho restituito anche l'ultima password.
            return password;
        }

        throw new NoSuchElementException();
    }

    /**
     * Scrive tutte le password sequenzialmente in un nuovo file di testo
     * (utilizzato per testare la correttezza della somma del numero al vettore. Sconsigliato un numero maggiore di
     * 3 per evitare di occupare un tempo troppo lungo ).
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final int PASSWORD_LENGTH = 2;

        PasswordGenerator p = new PasswordGenerator(PASSWORD_LENGTH, 0);
        FileOutputStream f = new FileOutputStream("test.txt");
        int i = 0;
        while (p.hasNext()) {
            i++;
            String password = p.next();
            String convertedPassword = IntStream.range(0, PASSWORD_LENGTH)
                    .mapToObj(j -> Byte.toString(password.getBytes()[j]))
                    .reduce("", (a,b) -> a + " " + b);
            f.write((i + ") " + convertedPassword + " | " + password + "\n").getBytes());
        }
    }
}
