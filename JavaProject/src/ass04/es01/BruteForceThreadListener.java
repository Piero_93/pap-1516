package ass04.es01;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 04 (per 2016-04-21)
 *
 * Assignment 4.01
 *
 * Listener per TesterThread
 */
public interface BruteForceThreadListener {

    /**
     * Metodo chiamato per inviare una notifica del completamento (con successo) della computazione
     * @param correctPassword password corretta
     */
    void computationEnded(String correctPassword);
}
