package ass05.es01;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Thread che esegue la ricerca del baricentro.
 */
public class WorkerCentroid extends Thread {
    private final List<Point2D> points;
    private final WorkerObserver observer;

    private final int start;
    private final int end;

    /**
    * Costruttore
    * @param observer listener per eseguire operaioni alla terminazione del thread
    * @param points lista dei punti che devono essere considerati* @param points
    * @param start inizio del range di punti da controllare
    * @param end termine del range di punti da controllare
    */
    public WorkerCentroid(WorkerObserver observer, List<Point2D> points, int start, int end) {
        this.observer = observer;
        this.points = points;
        this.start = start;
        this.end = end;
    }

    /**
     * Corpo del thread.
     * Esegue la ricerca del baricentro e notifica l'observer al termine.
     */
    @Override
    public void run() {
        observer.centroidComputationEnded(
                new BigDecimal(IntStream.range(start, end)
                        .mapToObj(points::get)
                        .map(Point2D::getX)
                        .reduce(0.0, (a, b) -> a + b)),
                new BigDecimal(IntStream.range(start, end)
                        .mapToObj(points::get)
                        .map(Point2D::getY)
                        .reduce(0.0, (a, b) -> a + b)),
                end - start
        );
    }
}
