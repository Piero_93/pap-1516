package ass05.es01;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Observer per i MinFinder (Parallelo e Sequenziale).
 */

public interface MinFinderObserver {

    /**
     * Metodo richiamato quando la ricerca del punto con distanza minima dal baricentro è terminata.
     * @param name nome dell'oggetto
     * @param centroid baricentro dei punti
     * @param best punto più vicino al baricentro
     * @param distance distanza del punto dal baricentro
     * @param timeElapsed tempo impiegato per la computazione
     */
    void computationEnded(String name, Point2D centroid, Point2D best, double distance, long timeElapsed);
}
