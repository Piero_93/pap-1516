package ass05.es01;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Tester (classe main)
 */
public class Tester {
    /**
     * Main
     * @param args determina il tipo di computazione desiderata e il numero di punti desiderati.
     *             Opzioni disponibili:
     *             1) Modalità di calcolo del risultato
     *                 - -seq: esecuzione sequenziale
     *                 - -par: esecuzione parallela
     *                 - -both: entrambe le esecuzioni, in sequenza
     *             2) Numero di punti. Se non specificato il numero di punti è 10.000.000.
     *             3) -waste: E' possibile scegliere se simulare un carico di lavoro più pesante inserendo il comando -waste.
     *                    In questo modo si riesce a fare in modo che il programma ci metta più tempo ad eseguire, riducendo
     *                    l'impatto che l'overhead per la creazione dei thread ha sul risultato finale.
     */
    public static void main(String[] args) {
        int numberOfPoints = 10000000;
        if(args.length < 1 || (!args[0].equals("-seq") && !args[0].equals("-par") && !args[0].equals("-both"))) {
            System.err.println("Usage: java Ass05.Es01.Tester [-seq/-par/-both] [-waste (optional]  [optional number of points] [-waste (optional)]");
            return;
        }

        if(args.length > 1 && (Integer.parseInt(args[1]) < Runtime.getRuntime().availableProcessors())) {
            System.err.println("Usage: java Ass05.Es01.Tester [-seq/-par/-both] [number of points (equals or greater than the number of core (" + Runtime.getRuntime().availableProcessors() + "))] [-waste (optional)]");
            return;
        } else if(args.length > 1){
            numberOfPoints = Integer.parseInt(args[1]);
        }

        if(args.length == 3 && args[2].equals("-waste")) {
            TimeWaster.waste = true;
        }


        Random r = new Random();

        System.out.println("Generating points...");
        try {
            List<Point2D> list = IntStream
                    .rangeClosed(1, numberOfPoints)
                    .mapToObj(i -> new Point2D(r.nextInt(1000), r.nextInt(1000)))
                    .collect(Collectors.toList());


            System.out.println("Press ENTER to continue");
            System.in.read();

            System.out.println("Starting with " + numberOfPoints + " points...\n");

            if (args[0].equals("-seq") || args[0].equals("-both")) {
                new SequentialMinFinder("Sequential", new MinFinderObserver() {
                    @Override
                    public void computationEnded(String name, Point2D centroid, Point2D best, double distance, long timeElapsed) {
                        System.out.println(name + " -> Nearest point to " + centroid + " is " + best + " with a distance of " + distance + "\nTime elapsed: " + timeElapsed + " msec\n");
                    }
                }, list);
            }
            if (args[0].equals("-par") || args[0].equals("-both")) {
                new ParallelMinFinder("Parallel", new MinFinderObserver() {
                    @Override
                    public void computationEnded(String name, Point2D centroid, Point2D best, double distance, long timeElapsed) {
                        System.out.println(name + " -> Nearest point to " + centroid + " is " + best + " with a distance of " + distance + "\nTime elapsed: " + timeElapsed + " msec\n");
                    }
                }, list);
            }
        } catch (OutOfMemoryError e) {
            System.err.println("Not enough space on memory. Please increase heap size.");
        } catch (IOException e) {}
    }


}
