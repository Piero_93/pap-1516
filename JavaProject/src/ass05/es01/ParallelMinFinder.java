package ass05.es01;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Ricerca del punto più vicino al baricentro in maniera parallela.
 */
public class ParallelMinFinder implements WorkerObserver {
    private final int numberOfThread;
    private int endedThread;

    private final long millisStart;
    private final MinFinderObserver observer;
    private final String name;

    private final List<Point2D> points;
    private Point2D centroid;
    private BigDecimal accumulatorX;
    private BigDecimal accumulatorY;
    private int alreadyConsidered;
    private Point2D bestPoint;

    /**
     * Costruttore
     * @param name nome dell'esecutore
     * @param observer observer che verrà richiamato al termine della computazione
     * @param points lista di punti su cui lavorare
     */
    public ParallelMinFinder(String name, MinFinderObserver observer, List<Point2D> points) {
        this.name = name;
        this.observer = observer;

        this.alreadyConsidered = 0;
        this.accumulatorX = new BigDecimal(0);
        this.accumulatorY = new BigDecimal(0);

        this.endedThread = 0;
        this.numberOfThread = Runtime.getRuntime().availableProcessors();

        this.points = points;

        this.millisStart = System.currentTimeMillis();

        int pointPerThread = points.size() / numberOfThread;

        IntStream.range(0, numberOfThread - 1)
                .forEach(i -> new WorkerCentroid(this, points, i * pointPerThread, (i + 1) *  pointPerThread).start());
        new WorkerCentroid(this, points, (numberOfThread - 1) * pointPerThread, points.size()).start();
    }

    @Override
    public synchronized void minDistanceComputationEnded(Point2D bestPoint) {
        if(this.bestPoint == null || bestPoint.getDistance(centroid) < this.bestPoint.getDistance(centroid)) {
            this.bestPoint = bestPoint;
        }

        endedThread++;
        if(endedThread == numberOfThread) {
            observer.computationEnded(name, centroid, this.bestPoint, this.bestPoint.getDistance(centroid), System.currentTimeMillis() - millisStart);
        }
    }

    @Override
    public synchronized void centroidComputationEnded(BigDecimal x, BigDecimal y, int numberOfPoints) {
        accumulatorX = accumulatorX.add(x);
        accumulatorY = accumulatorY.add(y);
        alreadyConsidered += numberOfPoints;

        if(alreadyConsidered == points.size()) {
            this.centroid = new Point2D(
                    accumulatorX.divide(new  BigDecimal(alreadyConsidered), 7, BigDecimal.ROUND_HALF_UP).doubleValue(),
                    accumulatorY.divide(new  BigDecimal(alreadyConsidered), 7, BigDecimal.ROUND_HALF_UP).doubleValue());
            int pointPerThread = points.size() / numberOfThread;

            IntStream.range(0, numberOfThread - 1)
                    .forEach(i -> new WorkerDistance(this, points, this.centroid, i * pointPerThread, (i + 1) * pointPerThread).start());
            new WorkerDistance(this, points, this.centroid, (numberOfThread - 1) * pointPerThread, points.size()).start();
        }
    }
}
