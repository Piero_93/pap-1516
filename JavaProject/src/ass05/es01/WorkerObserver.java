package ass05.es01;

import java.math.BigDecimal;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Observer per i thread.
 */
public interface WorkerObserver {

    /**
     * Metodo richiamato quando il thread per ricercare la distanza temrina la sua esecuzione.
     * @param bestPoint punto tra quelli di interesse del thread più vicino al baricentro
     */
    void minDistanceComputationEnded(Point2D bestPoint);

    /**
     * Metodo richiamato quando il thread per ricercare il baricentro termina la sua esecuzione.
     * @param x contributo delle x alla computazione del baricentro
     * @param y contributo delle y alla computazione del baricentro
     * @param numberOfPoints numero di punti considerati
     */
    void centroidComputationEnded(BigDecimal x, BigDecimal y, int numberOfPoints);
}
