package ass05.es01;

import javafx.util.Pair;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Thread che esegue la ricerca del punto con minima distanza dal baricentro.
 */
public class WorkerDistance extends Thread {
    private final List<Point2D> points;
    private final Point2D centroid;
    private final WorkerObserver observer;

    private final int start;
    private final int end;

    /**
     * Costruttore
     * @param observer listener per eseguire operaioni alla terminazione del thread
     * @param points lista dei punti
     * @param centroid baricentro (trovare la distanza minima da esso)
     * @param start inizio del range di punti da controllare
     * @param end termine del range di punti da controllare
     */
    public WorkerDistance(WorkerObserver observer, List<Point2D> points, Point2D centroid, int start, int end) {
        this.observer = observer;
        this.points = points;
        this.centroid = centroid;
        this.start = start;
        this.end = end;
    }

    /**
     * Corpo del thread.
     * Esegue la ricerca del punto con minima distanza dal baricentro e notifica l'observer terminata l'operazione.
     */
    @Override
    public void run() {
        observer.minDistanceComputationEnded(
                IntStream.range(start, end)
                        .mapToObj(i -> new Pair<>(points.get(i), points.get(i).getDistance(centroid)))
                        .min((pairA, pairB) -> (int) Math.signum(pairA.getValue() - pairB.getValue())).get().getKey()
        );
    }
}
