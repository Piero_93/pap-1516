package ass05.es01;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Rappresentazione di un punto sul piano.
 */
public class Point2D {
    private final double x;
    private final double y;

    /**
     * Costruttore
     * @param x coordinata x del punto
     * @param y coordinata y del punto
     */
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Restituisce la distanza tra il punto e uno passato come parametro
     * @param p punto da cui calcolare la distanza
     * @return la distanza tra i due punti
     */
    public double getDistance(Point2D p) {
        TimeWaster.wasteTime();
        return Math.sqrt(Math.pow(this.x - p.x, 2) + Math.pow(this.y - p.y, 2));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
