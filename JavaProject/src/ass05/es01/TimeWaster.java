package ass05.es01;

/**
 * Time waster.
 * Consuma tempo di CPU per simulare un carico di lavoro più pesante.
 */
public class TimeWaster {
    public static boolean waste = false;

    public static void wasteTime() {
        if (waste) {
            int a = 0;
            for (int i = 0; i < 100000; i++) {
                a += Math.sqrt(Math.pow(i, 2) + Math.pow(i, 2));
            }
            Math.floor(a);
        }
    }
}
