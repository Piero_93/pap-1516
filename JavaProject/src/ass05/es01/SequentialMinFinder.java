package ass05.es01;

import javafx.util.Pair;

import java.util.List;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Ricerca del punto più vicino al baricentro in maniera sequenziale.
 */
public class SequentialMinFinder {
    /**
     * Costruttore
     * @param name nome dell'esecutore
     * @param observer observer che verrà richiamato al termine della computazione
     * @param points lista di punti su cui lavorare
     */
    public SequentialMinFinder(String name, MinFinderObserver observer, List<Point2D> points) {
        long millisStart = System.currentTimeMillis();

        Point2D centroid = new Point2D(
                points.stream().map(Point2D::getX).reduce(0.0, (a, b) -> a + b)/ points.size(),
                points.stream().map(Point2D::getY).reduce(0.0, (a, b) -> a + b)/ points.size()
        );

        Point2D bestPoint = points.stream()
                .map(point -> new Pair<>(point, point.getDistance(centroid)))
                .min((pairA, pairB) -> (int) Math.signum(pairA.getValue() - pairB.getValue())).get().getKey();

        observer.computationEnded(
                name,
                centroid,
                bestPoint,
                bestPoint.getDistance(centroid),
                System.currentTimeMillis() - millisStart);
    }
}
