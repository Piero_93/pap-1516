package ass05.es02;

import java.util.concurrent.Semaphore;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Tester. Avvia l'esecuzione dei thread.
 */
public class Tester {

    /**
     * Main
     * @param args not used
     */
    public static void main(String[] args) {
        ICounter c1 = new Counter(0);
        ICounter c2 = new Counter(0);
        ICounter c3 = new Counter(0);
        ICounter c4 = new Counter(0);

        Semaphore s1 = new Semaphore(0);
        Semaphore s2 = new Semaphore(0);
        Semaphore s3 = new Semaphore(0);
        Semaphore s4 = new Semaphore(0);
        Semaphore s5 = new Semaphore(0);
        Semaphore s6 = new Semaphore(0);

        Semaphore mutex = new Semaphore(1);

        //Worker 1
        new Worker(
                s1::acquire,
                c1::inc,
                () -> {
                    s2.release();
                    s3.release();
                }
        ).start();

        //Worker 2
        new Worker(
                s2::acquire,
                c2::inc,
                s4::release
        ).start();

        //Worker 3
        new Worker(
                s3::acquire,
                c3::inc,
                s5::release
        ).start();

        //Worker 4
        new Worker(
                s4::acquire,
                () -> {
                    mutex.acquire();
                    System.out.println("c2: " + c2);
                    c4.inc();
                    mutex.release();
                },
                s6::release
        ).start();

        //Worker 5
        new Worker(
                s5::acquire,
                () -> {
                    mutex.acquire();
                    System.out.println("c3: " + c3);
                    c4.inc();
                    mutex.release();
                },
                s6::release
        ).start();

        //Worker 6
        new Worker(
                () -> {
                    s6.acquire();
                    s6.acquire();
                },
                () -> {
                    mutex.acquire();
                    System.out.println("c4: " + c4);
                    mutex.release();
                },
                s1::release
        ).start();

        //Sblocca il thread 1 per avviare l'intero programma.
        s1.release();
    }
}
