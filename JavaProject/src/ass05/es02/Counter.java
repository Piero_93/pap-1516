package ass05.es02;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Implementazione dei contatori
 */

public class Counter implements ICounter {
    private int value;

    /**
     * Costruttore vuoto.
     */
    public Counter() {
        value = 0;
    }

    /**
     * Costruttore con valore iniziale
     * @param value valore iniziale
     */
    public Counter(int value) {
        this.value = value;
    }

    @Override
    public void inc() {
        value++;
    }

    @Override
    public void dec() {
        value--;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
