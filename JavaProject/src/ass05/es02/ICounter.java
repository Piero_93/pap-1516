package ass05.es02;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Interfaccia di un contatore.
 */

public interface ICounter {

    /**
     * Aumento del valore
     */
    void inc();

    /**
     * Decremento del valore
     */
    void dec();
}
