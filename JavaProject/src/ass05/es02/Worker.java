package ass05.es02;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Generico worker in grado di eseguire alcune operazioni.
 */
public class Worker extends Thread {
    private final RunnableWithException before;
    private final RunnableWithException action;
    private final RunnableWithException after;

    /**
     * Costruttore
     * @param before operazione da eseguire per prima (es. attendere lo sblocco di un semaforo)
     * @param action operazione da eseguire (es. stampa di un valore)
     * @param after operazione da eseguire successivamente (es. sbloccare un semaforo)
     */
    public Worker(RunnableWithException before, RunnableWithException action, RunnableWithException after) {
        this.before = before;
        this.action = action;
        this.after = after;
    }

    /**
     * Corpo del thread. Esegue le tre operazioni in sequenza.
     */
    @Override
    public void run() {
        while(true) {
            try {
                before.execute();
                action.execute();
                after.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
