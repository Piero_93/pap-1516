package ass05.es02;

/**
 * Biagini Piero  - 772116
 * Programmazione Avanzata e Paradigmi
 * Assignment 05 (per 2016-05-14)
 *
 * Assignment 5.01
 *
 * Interfaccia per modellare le azioni dei thread.
 * Il metodo execute è in grado di lanciare eccezioni,
 *      quindi non è necessario utilizzare try catch per ogni operazione sui semafori.
 */

public interface RunnableWithException {
    void execute() throws Exception;
}
