package lab04.helloconcurrentworld;

/**
 * Created by piero.biagini on 15/04/2016.
 */
public class HelloConcurrentWorld extends Thread {
    public HelloConcurrentWorld(String name) {
        super(name);
    }

    public void run() {
        System.out.println("Hello concurrent world!");
    }
}
