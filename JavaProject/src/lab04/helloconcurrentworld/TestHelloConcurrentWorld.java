package lab04.helloconcurrentworld;

/**
 * Created by piero.biagini on 15/04/2016.
 */
public class TestHelloConcurrentWorld {
    public static void main(String[] args) {
        HelloConcurrentWorld thread = new HelloConcurrentWorld("My Thread");
        thread.start();

        String myName = Thread.currentThread().getName();
        try {
            Thread.currentThread().sleep(1000);
        } catch (InterruptedException e) {}
        System.out.println("Thread spawned - I'm \"" + myName + "\"");
    }
}
