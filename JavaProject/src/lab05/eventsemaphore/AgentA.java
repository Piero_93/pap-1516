package lab05.eventsemaphore;

import java.util.concurrent.Semaphore;

public class AgentA extends Thread {

    private final Semaphore ev;

    public AgentA(Semaphore ev) {
        this.ev = ev;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Hello");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
        ev.release();
    }
}
