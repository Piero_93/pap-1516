package lab05.eventsemaphore;

import java.util.concurrent.Semaphore;

public class AgentB extends Thread {

    private final Semaphore ev;

    public AgentB(Semaphore ev) {
        this.ev = ev;
    }

    @Override
    public void run() {
        try {
            ev.acquire();
        for (int i = 0; i < 10; i++) {
            System.out.println("world");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
        } catch (InterruptedException e) {}
    }
}