package lab05.eventsemaphore;

import java.util.concurrent.Semaphore;

/**
 * Created by Piero on 06/05/16.
 */
public class TestSynch {
    public static void main(String[] args) {
        Semaphore ev = new Semaphore(0);
        new AgentA(ev).start();
        new AgentB(ev).start();
    }
}
