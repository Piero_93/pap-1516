package lab05.ricci.factorizer;

public interface FactorizerService {

	int[] getFactors(long n);

}
