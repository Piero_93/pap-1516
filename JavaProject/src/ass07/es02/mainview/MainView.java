package ass07.es02.mainview;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Set;

/**
 * GUI principale
 */
public class MainView extends JFrame {
    private JLabel winner;
    private JButton stopButton;

    /**
     * Costruttore
     * @param controller controller della GUI, da notificare alla pressione dei bottoni
     */
    public MainView(Oracle controller) {
        this.setTitle("Guess The Number!");
        this.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                controller.kill();
            }
        });
        this.setMinimumSize(new Dimension(400, 70));
        this.setMaximumSize(new Dimension(400, 70));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        JSpinner playerNumberSpinner = new JSpinner();
        JButton startButton = new JButton("GO!");
        stopButton = new JButton("KILL");
        winner = new JLabel("Press \"GO!\" to play!");

        playerNumberSpinner.setValue(1);
        playerNumberSpinner.setPreferredSize(new Dimension(100, 30));
        this.add(playerNumberSpinner, gridBagConstraints);

        startButton.addActionListener((e) -> {
            if((Integer) playerNumberSpinner.getValue() > 0) {
                controller.initAndStart((Integer) playerNumberSpinner.getValue());
                startButton.setEnabled(false);
                playerNumberSpinner.setEnabled(false);
                stopButton.setEnabled(true);
                winner.setText("Game started!");
            } else {
                JOptionPane.showMessageDialog(this, "Please insert a number of player greater than 0!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
        this.add(startButton, gridBagConstraints);

        stopButton.addActionListener((e) -> {
            controller.kill();
            stopButton.setEnabled(false);
            winner.setText("Game stopped :(");
        });
        stopButton.setEnabled(false);
        this.add(stopButton, gridBagConstraints);

        this.add(winner);

        this.setVisible(true);
    }

    /**
     * Imposta i nomi dei vincitori sulla GUI
     * @param player identificatori dei vincitori
     */
    public void setWinners(Set<Integer> player) {
        SwingUtilities.invokeLater(() -> {
            winner.setText("Player " + player.stream().map((p) -> "" + p).reduce((s, p) -> s + " and " + p).get() + " won!");
            stopButton.setEnabled(false);
        });
    }
}
