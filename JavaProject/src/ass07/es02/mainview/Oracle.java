package ass07.es02.mainview;

import ass07.es02.player.Player;
import ass07.es02.utilities.Clue;
import ass07.es02.utilities.Console;
import ass07.es02.utilities.Synchronizer;

import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Oracolo: gestore del gioco.
 */
public class Oracle extends Thread {
    private final int SECRET_NUMBER;
    private final Set<Player> PLAYERS;
    private final MainView VIEW;

    private Synchronizer synchronizer;

    private int turnCounter;
    private Set<Integer> winners;

    private boolean stop;

    /**
     * Costruttore
     */
    public Oracle() {
        this.PLAYERS = new HashSet<>();

        this.SECRET_NUMBER = new Random().nextInt(100);
        Console.log("Secret number is " + SECRET_NUMBER + "\n");

        this.VIEW = new MainView(this);

        this.turnCounter = 0;
        this.winners = new HashSet<>();
    }

    /**
     * Inizializza e avvia il gioco
     * @param playerNumber numero di giocatori desiderati
     */
    public synchronized  void initAndStart(int playerNumber) {
        this.synchronizer = new Synchronizer(playerNumber);
        this.start();
        for (int i = 0; i < playerNumber; i++) {
            Player p = new Player(i + 1, synchronizer);
            this.PLAYERS.add(p);
            p.start();
        }
    }

    /**
     * Termina prematuramente il gioco, notificando a tutti i giocatori di fermarsi
     */
    public synchronized void kill() {
        this.stop = true;
        PLAYERS.forEach(Player::kill);
    }

    /**
     * Per ogni turno:
     *  - Incrementa il contatore dei turni
     *  - Attendi che tutti i player abbiano giocato
     *  - Valuta i numeri estratti dai player
     *  - Imposta i risultati nel monitor
     */
    @Override
    public void run() {
        while(!stop) {
            this.turnCounter++;
            Console.log("Turn " + turnCounter + " started");
            Map<Integer, Integer> results = synchronizer.getTurnResult();
            Console.log("Turn " + turnCounter + " ended\n");
            Map<Integer, Clue> clues = results.entrySet().stream().collect(Collectors.toMap(
                    Map.Entry::getKey,
                    e -> {
                        if(SECRET_NUMBER < e.getValue()) {
                            return Clue.SMALLER;
                        } else if(SECRET_NUMBER > e.getValue()) {
                            return Clue.BIGGER;
                        } else {
                            winners.add(e.getKey());
                            return Clue.EQUALS;
                        }
                    }
            ));
            if(winners.size() != 0) {
                PLAYERS.forEach((p) -> p.guessed(winners));
                VIEW.setWinners(winners);
                this.kill();
            }
            synchronizer.setCurrentClues(clues);
        }
    }
}

