package ass07.es02.mainview;

import java.util.Set;

/**
 * Listener per il MainController, utilizzato per aggiornare la grafica
 */
public interface MainControllerListener {
    /**
     * Notifica che qualcuno ha indovinato il numero segreto
     * @param players giocatore che ha indovinato
     */
    void guessed(Set<Integer> players);

    /**
     * Notifica la volontà di terminare prematuramente l'esecuzione
     */
    void kill();
}
