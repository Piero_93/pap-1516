package ass07.es02.player;

import ass07.es02.mainview.MainControllerListener;
import ass07.es02.utilities.Clue;
import ass07.es02.utilities.Console;
import ass07.es02.utilities.Synchronizer;

import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by piero.biagini on 27/05/2016.
 */
public class Player extends Thread implements MainControllerListener {
    private volatile boolean executing;
    private int playerNumber;
    private Synchronizer synchronizer;

    /**
     * Costruttore
     * @param playerNumber numero identificativo del giocatore
     * @param synchronizer monitor per sincronizzare i turni
     */
    public Player(int playerNumber, Synchronizer synchronizer) {
        this.playerNumber = playerNumber;
        this.synchronizer = synchronizer;
    }

    /**
     * Per ogni turno:
     *  - Estrai un numero
     *  - Sottometti il numero
     *  - Attendi fino a che non viene valutato
     *  - Valuta il numero
     */
    @Override
    public void run() {
        executing = true;

        int lowerBound = 0;
        int upperBound = Integer.MAX_VALUE;

        while (executing) {
            int number = ThreadLocalRandom.current().nextInt(lowerBound, upperBound); //Estrai un numero
            Console.log(playerNumber, "Trying with " + number + " (between " + lowerBound + " and " + upperBound + ")");
            Clue result = synchronizer.say(playerNumber, number);
            switch (result) {
                case BIGGER:
                    lowerBound = number;
                    break;
                case SMALLER:
                    upperBound = number;
                    break;
                case EQUALS:
                    Console.log(playerNumber, "I have guessed the number! It's " + number + "!");
                    break;
            }
        }
    }

    @Override
    public void guessed(Set<Integer> players) {
        this.kill();
        if(players.contains(playerNumber)) {
            Console.log(playerNumber, "I WON!! :)");
        } else {
            Console.log(playerNumber, ": I LOST!! :(");
        }
    }

    @Override
    public void kill() {
        executing = false;
    }
}
