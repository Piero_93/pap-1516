package ass07.es02.utilities;

import java.util.HashMap;
import java.util.Map;

/**
 * Monitor per la gestione della sincronizzazione tra Oracolo e Player
 */
public class Synchronizer {
    private Map<Integer, Integer> currentTurnNumbers;
    private Map<Integer, Clue> currentClues;

    private int numberOfPLayers;

    private volatile int doneInCurrentTurn;

    /**
     * Costruttore
     * @param numberOfPLayers numero di giocatori nella partita
     */
    public Synchronizer(int numberOfPLayers) {
        this.currentTurnNumbers = new HashMap<>();
        this.currentClues = new HashMap<>();
        this.numberOfPLayers = numberOfPLayers;
        this.doneInCurrentTurn = 0;
    }

    /**
     * Metodo chiamato dai player per tentare di indovinare il numero
     * @param player numero identificativo del player
     * @param number numero estratto
     * @return il suggerimento
     */
    public synchronized Clue say(int player, int number) {
        this.currentTurnNumbers.put(player, number);
        this.doneInCurrentTurn++;

        notifyAll(); //I player si sospenderanno nuovamente, ma se tutti hanno giocato l'oracolo valuterà i risultati
        while (this.currentTurnNumbers.get(player) != null) { //Se l'oracolo non ha valutato i risultati, il player si sospende
            try {
                wait();
            } catch (InterruptedException e) {}
        }

        return this.currentClues.get(player);
    }

    /**
     * Metodo chiamato dall'oracolo per ottenere i risultati di un turno
     * @return i risultati di un turno
     */
    public synchronized Map<Integer, Integer> getTurnResult() {
        while (doneInCurrentTurn < numberOfPLayers) { //Se non tutti i player hanno giocato, l'oracolo si sospende
            try {
                wait();
            } catch (InterruptedException e) {}
        }

        return this.currentTurnNumbers;
    }

    /**
     * Metodo chiamato dall'oracolo per impostare i risultati di un turno
     * @param clues suggerimenti per i player
     */
    public synchronized void setCurrentClues(Map<Integer, Clue> clues) {
        this.currentClues = clues;
        this.doneInCurrentTurn = 0;
        this.currentTurnNumbers.clear();
        notifyAll(); //L'oracolo si sospenderà fino a che tutti i player non avranno giocato, i player si svegliano
    }
}
