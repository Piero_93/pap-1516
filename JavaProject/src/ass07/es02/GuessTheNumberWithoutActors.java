package ass07.es02;

import ass07.es02.mainview.Oracle;

/**
 * Launcher per il programma
 */
public class GuessTheNumberWithoutActors {
    public static void main(String[] args) {
        new Oracle();
    }
}
