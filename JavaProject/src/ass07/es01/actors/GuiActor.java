package ass07.es01.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import ass07.es01.messages.EndingMessage;
import ass07.es01.messages.PlayersMessage;
import ass07.es01.messages.StartMessage;
import ass07.es01.messages.WinMessage;
import ass07.es01.utilities.Motivation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.Set;

/**
 * Attore AKKA che gestisce la GUI
 */
public class GUIActor extends UntypedActor {
    private Set<ActorRef> players;
    private ActorRef oracle;
    private MainView view;

    /**
     * Costruttore
     */
    public GUIActor() {
        this.players = new HashSet<>();
        this.view = new MainView();
    }

    /**
     * Metodo richiamato alla ricezione di un messaggio.
     * @param message messaggio ricevuto. Può essere:
     *                - WinMessage: scrive sulla GUI il nome o i nomi dei vincitori
     *                - Unhandled
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof WinMessage) { //Elenco dei vincitori
            view.setWinners(((WinMessage) message).getWinner());
            return;
        } else if (message instanceof EndingMessage) { //Terminazione dell'intero sistema alla chiusura della GUI
            System.out.println("\nShutting down! Bye!");
            terminateAll();
        }
        unhandled(message);
    }

    /**
     * Inizializza gli attori
     * @param playerNumber numero di player desiderati
     */
    private void initialize(int playerNumber) {
        oracle = getContext().system().actorOf(Props.create(OracleActor.class, this.getSelf()));

        for (int i = 0; i < playerNumber; i++) {
            ActorRef player = getContext().system().actorOf(Props.create(PlayerActor.class, i + 1, oracle));
            players.add(player);
        }

        //Invia l'elenco dei player all'oracolo
        oracle.tell(new PlayersMessage(players), this.getSelf());
    }

    private void terminateAll() {
        GUIActor.this.players.forEach((p) -> {
            if(!p.isTerminated()) {
                p.tell(new EndingMessage(Motivation.KILL), GUIActor.this.getSelf());
            }
        });
        if(!oracle.isTerminated()) {
            oracle.tell(new EndingMessage(Motivation.KILL), GUIActor.this.getSelf());
        }


        getContext().stop(getContext().parent());
    }

    /**
     * Classe che modella la GUI
     */
    private class MainView extends JFrame {
        private JLabel winner;
        private JButton stopButton;

        /**
         * Costruttore
         */
        public MainView() {
            this.setTitle("Guess The Number!");
            this.setLayout(new GridBagLayout());
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    super.windowClosing(e);
                    GUIActor.this.oracle.tell(new EndingMessage(Motivation.KILL), GUIActor.this.getSelf());
                    GUIActor.this.getSelf().tell(new EndingMessage(Motivation.KILL), GUIActor.this.getSelf());
                }
            });
            this.setMinimumSize(new Dimension(400, 70));
            this.setMaximumSize(new Dimension(400, 70));
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

            JSpinner playerNumberSpinner = new JSpinner();
            JButton startButton = new JButton("GO!");
            stopButton = new JButton("KILL");
            winner = new JLabel("Press \"GO!\" to play!");

            playerNumberSpinner.setValue(1);
            playerNumberSpinner.setPreferredSize(new Dimension(100, 30));
            this.add(playerNumberSpinner, gridBagConstraints);

            startButton.addActionListener((e) -> {
                if((Integer) playerNumberSpinner.getValue() > 0) {
                    GUIActor.this.initialize((Integer) playerNumberSpinner.getValue());
                    startButton.setEnabled(false);
                    playerNumberSpinner.setEnabled(false);
                    stopButton.setEnabled(true);
                    winner.setText("Game started!");
                } else {
                    JOptionPane.showMessageDialog(this, "Please insert a number of player greater than 0!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            });
            this.add(startButton, gridBagConstraints);

            stopButton.addActionListener((e) -> {
                GUIActor.this.getSelf().tell(new EndingMessage(Motivation.KILL), GUIActor.this.getSelf());
                stopButton.setEnabled(false);
                winner.setText("Game stopped :(");
            });
            stopButton.setEnabled(false);
            this.add(stopButton, gridBagConstraints);

            this.add(winner);

            this.setVisible(true);
        }

        /**
         * Imposta i nomi dei vincitori sulla GUI
         * @param player identificatori dei vincitori
         */
        public void setWinners(Set<Integer> player) {
            SwingUtilities.invokeLater(() -> {
                winner.setText("Player " + player.stream().map((p) -> "" + p).reduce((s, p) -> s + " and " + p).get() + " won!");
                stopButton.setEnabled(false);
            });
        }
    }
}

