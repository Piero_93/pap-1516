package ass07.es01.actors;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.UntypedActor;
import ass07.es01.messages.*;
import ass07.es01.utilities.Console;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Attore AKKA che modella il giocatore.
 * Per ogni turno, sottomette un numero e riceve un suggerimento a seconda che il numero sottomesso sia più grande o più piccolo di quello ricercato.
 */
public class PlayerActor extends UntypedActor {
    private int playerNumber;
    private int lowerBound;
    private int upperBound;
    private ActorRef master;

    /**
     * Costruttore
     * @param playerNumber numero del giocatore
     * @param master riferimento all'Oracolo (Attore a cui sottomettere il numero)
     */
    public PlayerActor(int playerNumber, ActorRef master) {
        this.playerNumber = playerNumber;
        this.lowerBound = 0;
        this.upperBound = Integer.MAX_VALUE;
        this.master = master;
    }

    /**
     * Metodo richiamato alla ricezione di un messaggio.
     * @param message messaggio ricevuto. Può essere:
     *                - StartMessage: triggera la sottomissione del primo numero
     *                - EndedTurnMessage: triggera la sottomissione dei successivi numeri
     *                - EndingMessage: triggera la terminazione dell'attore, scrivendo a schermo il motivo
     *                - ResponseMessage: riceve il suggerimento e aggiorna i suoi ati di conseguenza
     *                - Unhandled
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof StartMessage || message instanceof EndedTurnMessage) { //Messaggi di avvio
            int number = ThreadLocalRandom.current().nextInt(lowerBound, upperBound);
            Console.log(playerNumber, "Trying with " + number + " (between " + lowerBound + " and " + upperBound + ")");
            master.tell(new ExtractedNumberMessage(number, playerNumber), this.getSelf());
            return;
        } else if(message instanceof EndingMessage) { //Messaggio di terminazione
            switch (((EndingMessage) message).getMotivation()) {
                case WIN:
                    if (((EndingMessage) message).getWinner().contains(playerNumber)) {
                        Console.log(playerNumber, "I WON! :)");
                    } else {
                        Console.log(playerNumber, "I LOST! :(");
                    }
                    break;
                case KILL:
                    Console.log(playerNumber, "Shutting down...");
                    break;
            }
            this.getSelf().tell(PoisonPill.getInstance(), this.getSelf());
            return;
        } else if(message instanceof ResponseMessage) { //Messaggio di risposta alla sottomissione di un numero
            switch(((ResponseMessage) message).getClue()) {
                case BIGGER:
                    lowerBound = ((ResponseMessage) message).getNumber();
                    break;
                case SMALLER:
                    upperBound = ((ResponseMessage) message).getNumber();
                    break;
                case EQUALS:
                    Console.log(playerNumber, "I have guessed the number! It's " + ((ResponseMessage) message).getNumber() + "!");
                    break;
            }
            return;
        }
        unhandled(message);
    }
}