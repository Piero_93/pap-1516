package ass07.es01.actors;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.UntypedActor;
import ass07.es01.messages.*;
import ass07.es01.utilities.Clue;
import ass07.es01.utilities.Console;
import ass07.es01.utilities.Motivation;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Attore AKKA che modella l'oracolo.
 * Per ogni turno, riceve da ogni giocatore un numero e risponde con un suggerimento.
 * Quando il turno è terminato notifica gli attori.
 * Quando un attore indovina il numero notifica la GUI.
 */
public class OracleActor extends UntypedActor {
    private final int SECRET_NUMBER;
    private Set<ActorRef> players;
    private ActorRef view;
    private int alreadyDoneForThisTurn;
    private int turns;
    private Set<Integer> winners;

    /**
     * Costruttore
     * @param view riferimento all'attore che gestisce la GUI
     */
    public OracleActor(ActorRef view) {
        this.SECRET_NUMBER = new Random().nextInt(100);
        Console.log("Secret number is " + SECRET_NUMBER + "\n");
        this.alreadyDoneForThisTurn = 0;
        this.turns = 0;
        this.view = view;
        this.winners = new HashSet<>();
    }

    /**
     * Metodo richiamato alla ricezione di un messaggio.
     * @param message messaggio ricevuto. Può essere:
     *                - EndingMessage: triggera la terminazione dell'attore, scrivendo a schermo il motivo
     *                - ExtractedNumberMessage: riceve un numero estratto, risponderà con il suggerimento ed eventualmente con il messaggio di fine turno.
     *                          Notificherà la GUI se qualche giocatore ha indovinato il numero.
     *                - PlayersMessage: riceve la lista di giocatori e li avvia
     *                - Unhandled
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof EndingMessage && ((EndingMessage) message).getMotivation() == Motivation.KILL) { //Terminazione
            this.getSelf().tell(PoisonPill.getInstance(), this.getSelf());
            return;
        } else if(message instanceof ExtractedNumberMessage) { //Sottomissione di un nuovo numero
            if(((ExtractedNumberMessage) message).getNumber() == SECRET_NUMBER) { //Se il numero è giusto
                this.getSender().tell(new ResponseMessage(Clue.EQUALS, ((ExtractedNumberMessage) message).getNumber()), this.getSelf());
                this.winners.add(((ExtractedNumberMessage) message).getPlayer());
            } else if (((ExtractedNumberMessage) message).getNumber() < SECRET_NUMBER) { //Se il numero è troppo piccolo
                this.getSender().tell(new ResponseMessage(Clue.BIGGER, ((ExtractedNumberMessage) message).getNumber()), this.getSelf());
            } else if (((ExtractedNumberMessage) message).getNumber() > SECRET_NUMBER) { //Se il numero è troppo grande
                this.getSender().tell(new ResponseMessage(Clue.SMALLER, ((ExtractedNumberMessage) message).getNumber()), this.getSelf());
            }

            this.alreadyDoneForThisTurn++; //Incrementa il numero di giocatori che hanno giocato per questo turno

            if(alreadyDoneForThisTurn == players.size()) { //Se tutti hanno giocato
                Console.log("Turn " + this.turns + " ended\n");
                if(winners.size() != 0) { //Se qualcuno ha vinto, notifica la GUI e i giocatori
                    this.players.forEach((p) -> {
                        p.tell(new EndingMessage(Motivation.WIN, winners), OracleActor.this.getSelf());
                    });
                    view.tell(new WinMessage(winners), this.getSelf());
                } else { //Se nessuno ha vinto, triggera il prossimo turno
                    alreadyDoneForThisTurn = 0;
                    turns++;
                    Console.log("Turn " + this.turns + " started");
                    players.forEach((p) -> p.tell(EndedTurnMessage.getInstance(), this.getSelf()));
                }
            }
            return;
        } else if(message instanceof PlayersMessage) { //Elenco dei giocatori
            this.players = ((PlayersMessage) message).getPlayers();
            Console.log("Turn 0 started");
            players.forEach((p) -> p.tell(StartMessage.getInstance(), this.getSelf()));
            return;
        }
        unhandled(message);
    }
}
