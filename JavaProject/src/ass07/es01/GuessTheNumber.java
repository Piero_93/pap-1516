package ass07.es01;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import ass07.es01.actors.GUIActor;

/**
 * Launcher del gioco. Crea il sistema di attori e il master (Oracolo)
 */
public class GuessTheNumber {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("GuessTheNumber");
        system.actorOf(Props.create(GUIActor.class));
    }
}
