package ass07.es01.utilities;

/**
 * Rappresenta un possibile motivo per cui viene richiesta la terminazione
 */
public enum Motivation {
    WIN,
    KILL
}
