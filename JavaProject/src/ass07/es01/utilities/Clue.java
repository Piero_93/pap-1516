package ass07.es01.utilities;

/**
 * Rappresenta una relazione tra il numero estratto e quello reale. Utilizzata come suggerimento per i giocatori
 */
public enum Clue {
    BIGGER,
    SMALLER,
    EQUALS
}