package ass07.es01.utilities;

/**
 * Stampa su Console aggiungendo il nome dell'attore
 */
public class Console {
    /**
     * Stampa l'output di un attore
     * @param playerNumber numero identificativo del giocatore
     * @param text testo da stampare
     */
    public static void log(int playerNumber, String text) {
        System.out.println("[Player " + playerNumber + "]: " + text);
    }

    /**
     * Stampa l'output dell'oracolo
     * @param text testo da stampare
     */
    public static void log(String text) {
        System.out.println("[Oracle]: " + text);
    }
}
