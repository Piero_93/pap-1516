package ass07.es01.messages;

import ass07.es01.utilities.Motivation;

import java.util.Set;

/**
 * Created by Piero on 27/05/16.
 */
public class EndingMessage {
    private Motivation motivation;
    private Set<Integer> winner;

    public EndingMessage(Motivation motivation) {
        this.motivation = motivation;
    }

    public EndingMessage(Motivation motivation, Set<Integer> winner) {
        this.motivation = motivation;
        this.winner = winner;
    }

    public Motivation getMotivation() {
        return motivation;
    }

    public Set<Integer> getWinner() {
        return winner;
    }
}

