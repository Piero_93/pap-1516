package ass07.es01.messages;

import akka.actor.ActorRef;

import java.util.Set;

/**
 * Messaggio per comunicare l'insieme dei giocatori all'Oracolo
 */
public class PlayersMessage {
    private Set<ActorRef> players;

    /**
     * Costruttore
     * @param players l'insieme dei giocatori
     */
    public PlayersMessage(Set<ActorRef> players) {
        this.players = players;
    }

    /**
     * Ottiene l'insieme dei giocatori
     * @return l'insieme
     */
    public Set<ActorRef> getPlayers() {
        return players;
    }
}
