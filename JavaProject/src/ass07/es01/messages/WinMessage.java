package ass07.es01.messages;

import java.util.Set;

/**
 * Messaggio per comunicare i vincitori
 */
public class WinMessage {
    Set<Integer> winner;

    /**
     * Costruttore
     * @param winner insieme dei vincitori (coloro che all'ultimo turno hanno indovinato il numero)
     */
    public WinMessage(Set<Integer> winner) {
        this.winner = winner;
    }

    /**
     * Ottiene l'insieme dei vincitori
     * @return l'insieme
     */
    public Set<Integer> getWinner() {
        return winner;
    }
}
