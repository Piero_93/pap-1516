package ass07.es01.messages;

import ass07.es01.utilities.Clue;

/**
 * Messaggio inviato in risposta alla sottomissione del numero
 */
public class ResponseMessage {
    private final int number;
    Clue clue;

    /**
     * Costruttore
     * @param clue suggerimento da inviare
     * @param number numero relativo al suggerimento
     */
    public ResponseMessage(Clue clue, int number) {
        this.clue = clue;
        this.number =  number;
    }

    /**
     * Ottiene il suggerimento
     * @return il suggemimento
     */
    public Clue getClue() {
        return clue;
    }

    /**
     * Ottiene il numero
     * @return il numero
     */
    public int getNumber() {
        return number;
    }
}
