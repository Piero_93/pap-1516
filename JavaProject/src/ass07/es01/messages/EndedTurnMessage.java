package ass07.es01.messages;

/**
 * Messaggio per la fine del turno
 */
public final class EndedTurnMessage {
    private static EndedTurnMessage instance;

    private EndedTurnMessage() {}

    /**
     * Ottiene l'istanza del messaggio
     * @return l'istanza
     */
    public static EndedTurnMessage getInstance() {
        if(instance == null) {
            instance = new EndedTurnMessage();
        }
        return instance;
    }
}
