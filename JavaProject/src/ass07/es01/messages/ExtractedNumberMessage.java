package ass07.es01.messages;

/**
 * Messaggio per la sottomissione di un numero
 */
public class ExtractedNumberMessage {
    private int number;
    private int player;

    /**
     * Costruttore
     * @param number numero sottomesso
     * @param player giocatore mittente
     */
    public ExtractedNumberMessage(int number, int player) {
        this.number = number;
        this.player = player;
    }

    /**
     * Ottiene il numero sottomesso
     * @return il numero
     */
    public int getNumber() {
        return number;
    }

    /**
     * Ottiene il giocatore mittente
     * @return il giocatore
     */
    public int getPlayer() {
        return player;
    }
}
