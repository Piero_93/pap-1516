package ass07.es01.messages;

/**
 * Messaggio per l'avvio dei player
 */
public final class StartMessage {
    private static StartMessage instance;

    private StartMessage() {}

    /**
     * Ottiene l'istanza del messaggio
     * @return l'istanza
     */
    public static StartMessage getInstance() {
        if(instance == null) {
            instance = new StartMessage();
        }
        return instance;
    }
}
