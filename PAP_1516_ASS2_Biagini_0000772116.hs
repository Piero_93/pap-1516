import Screen
{-
Biagini Piero  - 772116
Programmazione Avanzata e Paradigmi
Assignment 02 (per 2016-xx-xx)
-}

{-
Assignment 2.01
Implementare la funzione:
longestDashSeq :: Code -> Maybe Int
La funzione deve determinare la lunghezza della sequenza di Dash più lunga nel Code specificato. Una sequenza di Dash può essere definita come o Single Dash (lunghezza 1) oppure da un (Comp Dash s), dove s è una sequenza di Dash, la cui lunghezza è pari a 1 + lunghezza s. Se Code non contiene Dash deve essere restituito Nothing.
-}

data Sym = Dot | Dash
data Code = Single Sym | Comp Sym Code

--Sequenze di esempio
testSequence1 :: Code
testSequence1 = (Single Dot)

testSequence2 :: Code
testSequence2 = (Single Dash)

testSequence3 :: Code
testSequence3 = (Comp Dash (Comp Dot (Comp Dash (Comp Dash (Single Dot)))))

testSequence4 :: Code
testSequence4 = (Comp Dash (Comp Dash (Comp Dash (Comp Dash (Comp Dash (Comp Dot (Comp Dash (Comp Dash (Comp Dash (Comp Dot (Comp Dash (Comp Dash (Comp Dash (Single Dash))))))))))))))

longestDashSeq :: Code -> Maybe Int
longestDashSeq (Single Dot) = Nothing
longestDashSeq (Single Dash) = Just 1
longestDashSeq  code = if (max (sequenceFinder code 0 []) 0) == 0 then Nothing else Just (max (sequenceFinder code 0 []) 0)
	where
		--Scansiona la sequenza di simboli e inserisce in una lista la lunghezza di ogni sequenza di dash
		sequenceFinder :: Code -> Int -> [Int] -> [Int]
		sequenceFinder (Single Dash) currSeq db = (currSeq + 1):db;
		sequenceFinder (Single Dot) currSeq db = currSeq:db
		sequenceFinder (Comp Dot tail) currSeq db = currSeq:db ++ (sequenceFinder tail 0 db)
		sequenceFinder (Comp Dash tail) currSeq db = db ++ (sequenceFinder tail (currSeq + 1) db)
		
		--Trova il massimo all'interno di una lista
		--In alternativa era possibile utilizzare la funzione findMax dell'assignment 2
		--Argomenti: (Lista), (Massimo corente)
		max :: [Int] -> Int -> Int
		max [] currMax = currMax
		max (x:xs) currMax
			| x > currMax = max xs x
			| otherwise = max xs currMax

{-
Assignment 2.02
Implementare la  funzione
findMax :: [Int] -> Maybe Int
che data una lista di interi l, determina - se esiste - il valore massimo utilizzando esclusivamente funzioni high-order di mapping o folding.
-}

findMax :: [Int] -> Maybe Int
findMax [] = Nothing
findMax (x:xs) = Just (foldr (\first second -> if first > second then first else second) x (x:xs))

{-
Assignment 2.03
Un Buyer rappresenta un utente che ha effettuato acquisti (ad esempio di un certo store online), caratterizzato da un certo identificatore e città di provenienza.  Transaction rappresenta una transazione di acquisto, effettuata da un certo buyer, un certo anno, per un certo ammontare.
Definire le seguenti funzioni di query, utilizzando funzioni high-order e composizioni di funzioni:
-}

type BuyerId = String
type City = String
type Year = Int
type Amount = Int
data Buyer = Buyer BuyerId City deriving (Show)
data Transaction = Trans BuyerId Year Amount deriving (Show)
data DBase = DBase [Buyer] [Transaction]


--Database di esempio
myDB = DBase 
	[(Buyer "B01" "City01"), (Buyer "B02" "City02"), (Buyer "B03" "City03"), (Buyer "B04" "City03")]
	[(Trans "B01" 2016 13), (Trans "B01" 2015 12),(Trans "B01" 2014 11),
		(Trans "B02" 2016 10), (Trans "B02" 2015 9), (Trans "B02" 2014 8),
		(Trans "B03" 2016 7), (Trans "B03" 2015 6), (Trans "B03" 2014 5),
		(Trans "B04" 2016 4), (Trans "B04" 2015 3), (Trans "B04" 2014 2)]
		
voidDB :: DBase
voidDB = DBase [] []

{-
Assignment 2.03.1
querySortedTransList :: DBase -> Year -> [Transaction]
Ottiene la lista delle transazioni riferite all’anno specificato, ordinate per amount
-}

--Versione meno efficiente con ordinamento eseguito con Selection-sort
querySortedTransList :: DBase -> Year -> [Transaction]
querySortedTransList (DBase buyers transactions) selYear =
	sort (filter (\(Trans buyerId year amount) -> year == selYear) transactions) [] (\(Trans fn fy fa) (Trans sn sy sa) -> fy < sy)
	where
		--Implementazione di insertion sort
		--Argomenti: (Lista da ordinare), (Parte di lista già ordinata), (Metodo di ordinamento)
		sort :: [a] -> [a] -> (a -> a -> Bool) -> [a]
		sort [] [] method = []
		sort [] list method = list
		sort (x:xs) db method = sort xs (insertInOrder db x method) method

		--Argomenti: (Lista ordinata), (Elemento da inserire), (Metodo di ordinamento: true se maggiore)
		insertInOrder :: [a] -> a -> (a -> a -> Bool) -> [a]
		insertInOrder [] current method = [current]
		insertInOrder (x:xs) current method
			| (method x current) = [x] ++ (insertInOrder xs current method)
			| otherwise = [current] ++ [x] ++ xs
			
--Versione più efficiente con ordinamento eseguito con Quicksort
querySortedTransListV2 :: DBase -> Year -> [Transaction]
querySortedTransListV2 (DBase buyers transactions) selYear =
	sort (filter (\(Trans buyerId year amount) -> year == selYear) transactions) (\(Trans fn fy fa) (Trans sn sy sa) -> fy < sy)
	where
		--Implementazione di quicksort
		--Argomenti: (Lista da ordinare), (Metodo di ordinamento: true se maggiore)
		sort :: [a] -> (a -> a -> Bool) -> [a]
		sort [] _ = []
		sort (x:xs) method = sort small method ++ (x : sort large method)
			where
				small = [y | y <- xs, not(method y x)]
				large = [y | y <- xs, method y x]


{-
Assignment 2.03.2
queryBuyerCities :: DBase -> [City]
Ottiene l’elenco delle città distinte dei  buyer
-}

queryBuyerCities :: DBase -> [City]
queryBuyerCities (DBase buyers transactions) =
	foldl (\output current -> output ++ (if member output current then [] else [current])) [] (map (\(Buyer buyerId city) -> city) buyers)
	where
		--Restituisce True se l'elemento è già presente in lista, altrimenti false
		--Argomenti: (Lista), (Elemento)
		member :: [City] -> City -> Bool
		member [] city = False
		member (x:xs) city = if x == city then True else (member xs city)
		
{-
Assignment 2.03.3
queryAmountsFromCity :: DBase -> City -> Amount
Determina la somma delle transazioni eseguite da buyer di una certa città
-}

queryAmountsFromCity :: DBase -> City -> Amount
queryAmountsFromCity (DBase buyers transactions) selCity =
	foldl (\sum (Trans idT year amount) -> if ((getBuyerCity idT buyers) == selCity) then sum + amount else sum) 0 transactions
	where
		--Ottiene la città dell'utente con un certo ID
		--Argomenti: (ID dell'utente), (Elenco degli utenti)
		getBuyerCity :: BuyerId -> [Buyer] -> City
		getBuyerCity currBuyerId [] = ""
		getBuyerCity currBuyerId ((Buyer buyerId city):ss)
			| currBuyerId == buyerId = city
			| otherwise = getBuyerCity currBuyerId ss
			
			
{-
Assignment 2.04
E’ dato un modulo Screen (presente nei sorgenti del corso) che contiene - fra gli altri - i seguenti tipi:
type Pos = (Int, Int)
data Color = BLACK | RED | GREEN | YELLOW | BLUE | MAGENTA | CYAN | WHITE
data Viewport = Viewport Pos Int Int
che rappresentano una posizione (x,y) di una viewport a caratteri (es: terminale), un colore e una viewport stessa di cui si specifica il vertice in alto a sinistra, larghezza w e altezza h.
A partire da questi tipi, definire il tipo TObj (che sta per “textual object” o “terminal object”) che può essere :
una stringa di testo s di colore c posizionata in una certa posizione p
una linea orizzontale di n caratteri ‘-’, a partire da una posizione p, di colore c
una linea verticale di n caratteri ‘|’, a partire da una posizione p, di colore c
un rettangolo, specificando vertice top-left, larghezza w e altezza h e colore
un box - come rettangolo pieno - specificando vertice top-left, larghezza w e altezza h e colore
Il tipo deve essere istanza della classe Show (già nota) e di una nuova classe Viewable o, a che supporta le seguenti funzioni

render :: Viewport -> o ->  IO ()
dato una Viewport e un elemento Viewable determina l’azione che ne esegue il rendering su terminale, considerando  come viewport quella specificata (per cui le posizioni degli elementi sono da considerare relativi alla viewport)

renderWithClipping :: Viewport -> o ->  IO ()
come render, ma con clipping

renderAll :: Viewport -> [o] ->  IO ()
dato una Viewport e una lista di Viewable determina l’azione che esegue il rendering di tutte gli elementi sulla view port
-}

{-
Nota: il programma è ottimizzato per funzionare con il terminale di Windows (sfondo BLACK, scritte WHITE).
Al temrine di ogni operazione riporta infatti i colori in tale configurazione.
Ovviamente è possibile modificare in maniera molto semplice i colori "di default" sostituendo il colore desiderato nella riga con il commento corrispondente.
-}

--DATI DI ESEMPIO
--Viewport d'esempio
testViewport :: Viewport
testViewport = Viewport (3,3) 50 50

--Rettangolo grande quanto il viewport, posizionato nello stesso punto
viewportBox :: TObj
viewportBox = Box 50 50 WHITE (0,0)

--Box più grande del viewport
biggerBox :: TObj
biggerBox = Box 50 50 RED (4,5)

--Rettangolo più grande del viewport
biggerRectangle :: TObj
biggerRectangle = Rectangle 40 40 CYAN (4,5)

--Rettangolo più piccolo del viewport
magentaRectangle :: TObj
magentaRectangle = Rectangle 6 3 MAGENTA (1,1)

--Riga orizzontale più grande del viewport
yellowHLine :: TObj
yellowHLine = HorizontalLine 80 YELLOW (4,3)

--Riga verticale più grande del viewport
redVLine :: TObj
redVLine = VerticalLine 200 RED (5,3)

--Lista di elementi utilizzabile con renderAll o renderAllWithClipping
listOfShapes :: [TObj]
listOfShapes = [viewportBox, biggerBox, biggerRectangle, magentaRectangle, yellowHLine, redVLine]

instance Show Color where
	show BLACK = "Black"
	show RED = "Red"
	show GREEN = "Green"
	show YELLOW = "Yellow"
	show BLUE = "Blue"
	show MAGENTA = "Magenta"
	show CYAN = "Cyan"
	show WHITE = "White"

--Tipo di dato TObj
data TObj =
	ColoredString String Color Pos --ColoredString (testo, colore, posizione): stringa colorata
	| HorizontalLine Int Color Pos --HorizontalLine (lunghezza, colore, posizione): riga orizzontale
	| VerticalLine Int Color Pos --VerticalLine (lunghezza, colore, posizione): riga verticale
	| Rectangle Int Int Color Pos --Rectangle (larghezza, altezza, colore, posizione dell'angolo): rettangolo
	| Box Int Int Color Pos --Rectangle (larghezza, altezza, colore, posizione dell'angolo): box
		deriving(Show)

--Dichiarazione della classe
class Viewable o where
	render :: Viewport -> o ->  IO ()
	renderWithClipping :: Viewport -> o ->  IO ()
	renderAll :: Viewport -> [o] ->  IO ()
	renderAllWithClipping :: Viewport -> [o] ->  IO () --Metodo aggiunto a fini di dimostrazione

draw :: IO () -> IO ()
draw objects =
	do
		objects
		setBColor BLACK --Colore di sfondo di default
		setFColor WHITE --Colore del testo di default
		goto (0,0)
	
	
instance Viewable TObj where
	--RENDER
	--String
	render (Viewport (viewportX, viewportY) width height) (ColoredString string color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| otherwise = draw (setBColor BLACK >> goto (viewportX + posX, viewportY + posY) >> setFColor color >> putStrLn string >> goto (0, 0))

	--HorizontalLine
	render (Viewport (viewportX, viewportY) width height) (HorizontalLine lineLength color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| lineLength <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor BLACK >> setFColor color >> foldl (>>) (return ())
			[writeAt (viewportX + posX + i, viewportY + posY) "-" | i <- [0..(lineLength - 1)]] >> goto (0, 0))

	--VerticalLine
	render (Viewport (viewportX, viewportY) width height) (VerticalLine lineLength color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| lineLength <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor BLACK >> setFColor color >> foldl (>>) (return ())
			[writeAt (viewportX + posX, viewportY + posY + i) "|" | i <- [0..(lineLength - 1)]] >> goto (0, 0))

	--Rectangle
	render _ (Rectangle 0 _ _ _) = return()
	render _ (Rectangle _ 0 _ _) = return()
	render (Viewport (viewportX, viewportY) width height) (Rectangle recW recH color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| recW <= 0 || recH <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor color >> foldl (>>) (return ())
			[writeAt (viewportX + posX, viewportY + posY + i) " " | i <- [0..(recH - 1)]] >>
			foldl (>>) (return ())
				[writeAt (viewportX + posX + i, viewportY + posY) " " | i <- [0..(recW - 1)]] >>
					foldl (>>) (return ())
						[writeAt (viewportX + posX + (recW - 1), viewportY + posY + i) " " | i <- [0..(recH - 1)]] >>
							foldl (>>) (return ())
								[writeAt (viewportX + posX + i, viewportY + posY + (recH -1)) " " | i <- [0..(recW - 1)]] >>
									goto (0, 0))

	--Box
	render _ (Box 0 _ _ _) = return()
	render _ (Box _ 0 _ _) = return()
	render (Viewport (viewportX, viewportY) width height) (Box recW recH color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| recW <= 0 || recH <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor color >> foldl (>>) (return ())
			[writeAt (viewportX + posX + j, viewportY + posY + i) " " | i <- [0..(recW - 1)], j <- [0..(recH - 1)]] >> goto (0, 0))
			
	--RENDER WITH CLIPPING
	--String
	renderWithClipping (Viewport (viewportX, viewportY) width height) (ColoredString string color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| posY >= height || posY < 0 = return()
		| otherwise = draw (setBColor BLACK >> setFColor color >>
			writeAt (viewportX + (if posX < 0 then 0 else posX), viewportY + posY)
				(buildString string width posX) >> goto (0, 0))
		where
			--Taglia la coda della stringa se essa esce dal viewport
			--Argomenti: (Larghezza del viewport), (Offset della stringa rispetto al viewport), (Lunghezza della stringa)
			buildString :: String -> Int -> Int -> String
			buildString [] _ _ = []
			buildString (x:xs) width xOffset
				| xOffset >= 0 && xOffset < width = [x] ++ (buildString xs width (xOffset + 1)) 
				| otherwise = buildString xs width (xOffset + 1)
 	
	--VerticalLine
	renderWithClipping (Viewport (viewportX, viewportY) width height) (VerticalLine lineLength color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| lineLength <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor BLACK >> setFColor color >> foldl (>>) (return ())
			[writeAt (x, y + i) "|" | i <- [0..(lineLength - 1)],
				isDotInViewPort (x, y + i) (Viewport (viewportX, viewportY) width height)] >> goto (0, 0))
		where
			--Coordinata x
			x = viewportX + posX
			--Coordinata y
			y = viewportY + posY
			
			--Restituisce True se il punto appartiene al viewport, altrimenti False
			--Argomenti: (Posizione del punto), (Viewport)
			isDotInViewPort :: Pos -> Viewport -> Bool
			isDotInViewPort (x,y) (Viewport (viewportX, viewportY) width height)
				| x >= viewportX && y >= viewportY && x < viewportX + width && y < viewportY + height = True
				| otherwise = False

			
	--HorizontalLine
	renderWithClipping (Viewport (viewportX, viewportY) width height) (HorizontalLine lineLength color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| lineLength <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor BLACK >> setFColor color >> foldl (>>) (return ())
			[writeAt (x + i, y) "-" | i <- [0..(lineLength - 1)],
				isDotInViewPort (x + i, y) (Viewport (viewportX, viewportY) width height)] >> goto (0, 0))
		where
			--Coordinata x
			x = viewportX + posX
			--Coordinata y
			y = viewportY + posY
			
			--Restituisce True se il punto appartiene al viewport, altrimenti False
			--Argomenti: (Posizione del punto), (Viewport)
			isDotInViewPort :: Pos -> Viewport -> Bool
			isDotInViewPort (x,y) (Viewport (viewportX, viewportY) width height)
				| x >= viewportX && y >= viewportY && x < viewportX + width && y < viewportY + height = True
				| otherwise = False


	--Rectangle
	renderWithClipping _ (Rectangle 0 _ _ _) = return()
	renderWithClipping _ (Rectangle _ 0 _ _) = return()
	renderWithClipping (Viewport (viewportX, viewportY) width height) (Rectangle recW recH color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| recW <= 0 || recH <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor color >> foldl (>>) (return ())
			[writeAt (x, y + i) " " | i <- [0..(recH - 1)],
				isDotInViewPort (x, y + i) (Viewport (viewportX, viewportY) width height)] >>
					foldl (>>) (return ()) [writeAt (x + i, y) " " | i <- [0..(recW - 1)],
						isDotInViewPort (x + i, y) (Viewport (viewportX, viewportY) width height)] >>
							foldl (>>) (return ()) [writeAt (x + (recW - 1), y + i) " " | i <- [0..(recH - 1)],
								isDotInViewPort (x + (recW - 1), y + i) (Viewport (viewportX, viewportY) width height)] >>
									foldl (>>) (return ()) [writeAt (x + i, y + (recH -1)) " " | i <- [0..(recW - 1)],
										isDotInViewPort (x + i, y + (recH-1)) (Viewport (viewportX, viewportY) width height)] >>
											goto (0, 0))
		where
			--Coordinata x
			x = viewportX + posX
			--Coordinata y
			y = viewportY + posY
			
			--Restituisce True se il punto appartiene al viewport, altrimenti False
			--Argomenti: (Posizione del punto), (Viewport)
			isDotInViewPort :: Pos -> Viewport -> Bool
			isDotInViewPort (x,y) (Viewport (viewportX, viewportY) width height)
				| x >= viewportX && y >= viewportY && x < viewportX + width && y < viewportY + height = True
				| otherwise = False

	
	--Box
	renderWithClipping _ (Box 0 _ _ _) = return()
	renderWithClipping _ (Box _ 0 _ _) = return()
	renderWithClipping (Viewport (viewportX, viewportY) width height) (Box recW recH color (posX, posY))
		| width < 0 || height < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare un viewport con dimensioni negative")
		| viewportX + posX < 0 || viewportY + posY < 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti a coordinate negative")
		| recW <= 0 || recH <= 0 = draw (setFColor RED >> writeAt (5,5) "Impossibile disegnare oggetti con dimensioni negative")
		| otherwise = draw (setBColor color >> foldl (>>) (return ())
			[writeAt (x + j, y + i) " " | i <- [0..(recW - 1)], j <- [0..(recH - 1)],
				isDotInViewPort (x + j, y + i) (Viewport (viewportX, viewportY) width height)] >> goto (0, 0))
		where
			--Coordinata x
			x = viewportX + posX
			--Coordinata y
			y = viewportY + posY
			
			--Restituisce True se il punto appartiene al viewport, altrimenti False
			--Argomenti: (Posizione del punto), (Viewport)
			isDotInViewPort :: Pos -> Viewport -> Bool
			isDotInViewPort (x,y) (Viewport (viewportX, viewportY) width height)
				| x >= viewportX && y >= viewportY && x < viewportX + width && y < viewportY + height = True
				| otherwise = False

			
	--RENDER ALL
	renderAll viewport [] = goto (0, 0)
	renderAll viewport (x:xs) = render viewport x >> renderAll viewport xs
	
	--RENDER ALL WITH CLIPPING
	renderAllWithClipping viewport [] = goto (0, 0)
	renderAllWithClipping viewport (x:xs) = renderWithClipping viewport x >> renderAllWithClipping viewport xs