{-
Biagini Piero  - 772116
Programmazione Avanzata e Paradigmi
Assignment 01 (per 2016-03-16)
-}

{-
Assignment 1.01
Implementare una funzione isSorted che data una lista di interi l restituisce vero o falso a seconda che l sia ordinata in ordine crescente oppure non lo sia.
-}

isSorted :: [Int] -> Bool
isSorted [] = True
isSorted ([x]) = True
isSorted (x:y:ss)
	| x <= y = isSorted (y:ss)
	| otherwise = False

{-
Assignment 1.02
Implementare la  funzione
occSimple :: [String] -> String -> [Int]
che data una lista di stringhe l e una stringa s, determina tutte le posizioni (occorrenze) in cui s compare in l. La prima posizione ha indice zero.
-}

--Lista di esempio
sampleList :: [String]
sampleList = ["a","aa","b","aa", "b", "a"]

occSimple :: [String] -> String -> [Int]
occSimple [] _ = []
occSimple list string = match list string
	where
		--Inc: incrementa di 1 tutti i valori della lista
		inc :: [Int] -> [Int]
		inc [] = []
		inc (x:xs) = (x + 1) : inc xs

		--Match: cerca le stringhe corrispondenti a quella in input
		match :: [String] -> String -> [Int]
		match [] _ = []
		match (x:xs) string
			| x == string = [0] ++ inc (match xs string)
			| otherwise = inc (match xs string)

		

{-
Assignment 1.03
Implementare la  funzione
occFull :: [String] -> [(String, [Int])]
che data una lista di stringhe l determina le occorrenze di tutte le stringhe contenute in l, determinando, per ogni stringa, tutte le posizioni in cui essa compare in l.
-}

--Lista di esempio
sampleList2 :: [String]
sampleList2 = ["a","aa","b","aa","b","c"]

occFull :: [String] -> [(String,[Int])]
occFull [] = []
occFull (x:xs) =
	let
		-- Cerca una stringa e incrementa il record
		inc :: [(String,[Int])] -> String -> Int -> [(String,[Int])]
		inc [] current index = [(current, [index])]
		inc ((record, occourences):ss) current index
			| record == current = [(record, (occourences ++ [index]))] ++ ss
			| otherwise = [(record, occourences)] ++ inc ss current index

		-- Scorre tutta la lista
		scanner :: [String] -> Int -> [(String,[Int])] -> [(String,[Int])]
		scanner [] _ result = result
		scanner (y:ys) index result = scanner ys (index + 1) (inc result y index)
	in
		scanner (x:xs) 0 []

{-
Assignment 1.04
Siano dati i seguenti tipi:
data Sym = Dot | Dash
data Code = Single Sym | Comp Sym Code
Implementare la funzione:
countDash :: Code -> Int
che dato un valore di tipo Code, conta il numero di elementi Dash in esso presenti.
-}

data Sym = Dot | Dash
data Code = Single Sym | Comp Sym Code

testDashDot :: Code
testDashDot = (Comp Dash (Single Dash))

countDash :: Code -> Int
countDash (Single Dash) = 1
countDash (Single Dot) = 0
countDash (Comp Dash code) = 1 + countDash code
countDash (Comp Dot code) = countDash code

{-
Assignment 1.05
Dato il tipo Code definito in precedenza, implementare la funzione
show :: Code -> String
che restituisce una rappresentazione testuale di Code, ove Dot è rappresentato dal carattere ‘.’ e Dash dal carattere ‘-’.
-}
--Nota: cambiato nome perchè la funzione show esiste già

showDashNDot :: Code -> String
showDashNDot (Single Dash) = "-"
showDashNDot (Single Dot) = "."
showDashNDot (Comp Dash code) = "-" ++ showDashNDot code
showDashNDot (Comp Dot code) = "." ++ showDashNDot code

{-
Assignment 1.06
Siano dati  i tipi:
data Digit = Zero | One
type BNum = [Digit]
dove BNum rappresenta un numero binario ad N cifre (interpretando l’elemento Zero come il valore 0 e l’elemento One come il valore 1), dove la prima posizione nella lista rappresenta la cifra più significativa (non necessariamente zero).
Implementare la funzione
equalsBNum :: BNum -> BNum -> Bool
che dati due BNum determina se rappresentano lo stesso numero binario.
-}

data Digit = Zero | One
type BNum = [Digit]

--Nota: 
-- Le stringhe vengono invertite prima di essere comparate in modo da scartare gli eventuali 0 non significativi iniziali

equalsBNum :: BNum -> BNum -> Bool
equalsBNum a b = compare (rev a) (rev b)
	where
		--Compare: esegue la comparazione per ogni singolo bit
		compare [] [] = True
		compare [] [Zero] = True
		compare [Zero] [] = True
		compare [One] [] = False
		compare [] [One] = False
		compare (x:xs) [] = compare xs []
		compare [] (y:ys) = compare [] ys
		compare (Zero:xs) (Zero:ys) = compare xs ys
		compare (One:xs) (One:ys) = compare xs ys
		compare (Zero:xs) (One:ys) = False
		compare (One:xs) (Zero:ys) = False
		
		--Reverse: inverte la lista di digit
		rev :: [a] -> [a]
		rev [] = []
		rev (x:xs) = (rev xs) ++ [x]

{-
Assignment 1.07
Dato il tipo BNum precedentemente definito, implementare la funzione
convBNum :: BNum -> Int
che ne determina il valore come numero intero.
-}

convBNum :: BNum -> Int
convBNum n =
	let
		--Reverse: inverte la lista di digit
		rev :: [a] -> [a]
		rev [] = []
		rev (x:xs) = (rev xs) ++ [x]

		--Convert: esegue la conversione
		conv :: BNum -> Int
		conv [] = 0
		conv [One] = 1
		conv [Zero] = 0
		conv (One:tail) = 1 + 2 * (conv tail)
		conv (Zero:tail) = 0 + 2 * (conv tail)
	in
		conv (rev n)

{-
Assignment 1.08
Dato il tipo BNum definito in precedenza, implementare la funzione
sumBNum :: BNum -> BNum -> BNum
che dati due numeri binari determina il numero binario che rappresenta la somma.
-}

sumBNum :: BNum -> BNum -> BNum
sumBNum first second = 
	let
		--Reverse: inverte la stringa
		rev :: [a] -> [a]
		rev [] = []
		rev (x:xs) =  rev xs ++ [x]

		--Half Adder: esegue la somma accettanto in input i due addendi e il riporto
		adder :: BNum -> BNum -> Digit -> BNum
		--Solo Carry In
		adder [] [] Zero = []
		adder [] [] One = [One]
		--Primo addendo e Carry In
		adder (Zero:xs) [] Zero = [Zero] ++ xs
		adder (Zero:xs) [] One = [One] ++ xs
		adder (One:xs) [] Zero = [One] ++ xs
		adder (One:xs) [] One = [Zero] ++ adder xs [] One 
		--Secondo addendo e Carry In
		adder [] (Zero:xs) Zero = [Zero] ++ xs
		adder [] (Zero:xs) One = [One] ++ xs
		adder [] (One:xs) Zero = [One] ++ xs
		adder [] (One:xs) One = [Zero] ++ adder xs [] One 
		--Primo e Secondo addendo e Carry In
		adder (Zero:xs) (Zero:ys) Zero = [Zero] ++ adder xs ys Zero
		adder (Zero:xs) (Zero:ys) One = [One] ++ adder xs ys Zero
		adder (One:xs) (Zero:ys) Zero = [One] ++ adder xs ys Zero
		adder (One:xs) (Zero:ys) One = [Zero] ++ adder xs ys One
		adder (Zero:xs) (One:ys) Zero = [One] ++ adder xs ys Zero
		adder (Zero:xs) (One:ys) One = [Zero] ++ adder xs ys One
		adder (One:xs) (One:ys) Zero = [Zero] ++ adder xs ys One
		adder (One:xs) (One:ys) One = [One] ++ adder xs ys One
	in
		rev(adder (rev first) (rev second) Zero)

--Show Digit
-- Funzione implementata per mostrare a schermo i numeri binari a fini di test
showBNum :: BNum -> String
showBNum [] = []
showBNum (Zero:xs) = "0" ++ showBNum xs
showBNum (One:xs) = "1" ++ showBNum xs

{-
Assignment 1.09
Dato il tipo Digit definito in precedenza e il tipo
data BTree a = Nil | Node a (BTree a) (BTree a)
che rappresenta un albero binario implementare la funzione:
countZeroInTree :: BTree Digit -> Int
che conta il numero di elementi Zero presenti nell’albero passato come parametro.
-}

data BTree a = Nil | Node a (BTree a) (BTree a)

--Albero di esempio
testBinaryTree :: BTree Digit
testBinaryTree = (Node Zero
			(Node One
				(Node One Nil Nil)
				(Node Zero Nil  Nil))
			(Node Zero
				(Node Zero Nil Nil)
				Nil ))

countZeroInTree :: BTree Digit -> Int
countZeroInTree Nil = 0
countZeroInTree (Node Zero left right) =  1 + countZeroInTree left + countZeroInTree right
countZeroInTree (Node One left right) = countZeroInTree left + countZeroInTree right

{-
Assignment 1.10
Dato il tipo BTree definito in precedenza, supponendo che rappresenti un albero binario di ricerca, implementare la funzione
getValuesLessThan :: BTree Int -> Int -> [Int]
che, dato un albero t e un valore v, determina la lista degli elementi presenti in t che hanno un valore inferiore a v.
-}

--Albero di esempio
testTree :: BTree Int
testTree = (Node 5
			(Node 2
				(Node 1 Nil Nil)
				(Node 3 Nil  Nil))
			(Node 7
				(Node 6 Nil Nil)
				Nil ))
	

getValuesLessThan :: BTree Int -> Int -> [Int]
getValuesLessThan Nil _ = []
getValuesLessThan (Node currentValue left right) value
	| value > currentValue = getValuesLessThan left value ++ [currentValue] ++ getValuesLessThan right value
	| otherwise = getValuesLessThan left value