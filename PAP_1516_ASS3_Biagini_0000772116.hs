{-
Biagini Piero  - 772116
Programmazione Avanzata e Paradigmi
Assignment 03 (per 2016-04-08)
-}

{-
Dato il tipo di dato che rappresenta un albero binario di ricerca:
data BSTree a = Nil | Node a (BSTree a) (BSTree a)
Implementare le funzioni:
bstMap :: (a -> b) -> BSTree a -> BSTree b
bstFold :: (b -> a -> b) -> b -> BSTree a -> b
bstFilter :: (a -> Bool) -> BSTree a -> BSTree a
bstForEach :: (a -> IO ()) -> BSTree a -> IO ()
che implementano rispettivamente la map, foldl, filter, foreach-do-action sulla struttura dati BSTree definita in precedenza.
-}

data BSTree a = Nil | Node a (BSTree a) (BSTree a) deriving (Show)

testTree :: BSTree Int
testTree = (Node 5
			(Node 2
				(Node 1 Nil Nil)
				(Node 3 Nil  Nil))
			(Node 7
				(Node 6 Nil Nil)
				Nil ))

biggerBst :: BSTree Int
biggerBst = (Node 60
				(Node 41
					(Node 16
						Nil
						(Node 25
							Nil
							Nil))
					(Node 53
						(Node 46
							(Node 42
								Nil
								Nil)
							Nil)
						(Node 55
							Nil
							Nil)))
				(Node 74
					(Node 65
						(Node 63
							(Node 62
								Nil
								Nil)
							(Node 64
								Nil
								Nil))
						(Node 70
							Nil
							Nil))
					Nil))

bstMap :: (a -> b) -> BSTree a -> BSTree b
bstMap _ Nil = Nil
bstMap mapF (Node curr left right) = Node (mapF curr) (bstMap mapF left)  (bstMap mapF right)

bstFold :: (b -> a -> b) -> b -> BSTree a -> b
bstFold _ current Nil = current
bstFold foldF current (Node curr left right) = bstFold foldF (foldF (bstFold foldF current left) curr) right

bstFilter :: (a -> Bool) -> BSTree a -> BSTree a
bstFilter _ Nil = Nil
bstFilter filterF (Node curr Nil Nil)
	| filterF curr = (Node curr Nil Nil)
	| otherwise = Nil
bstFilter filterF (Node curr left right)
	| filterF curr = (Node curr (bstFilter filterF left) (bstFilter filterF right))
	| otherwise = rebuildLowerTree (Node curr (bstFilter filterF left) (bstFilter filterF right))
	where
		--Ottiene l'elemento più grande del sottoalbero sinistro (ovvero quello più a destra)
		getMaxFromLeft :: BSTree a -> a
		getMaxFromLeft (Node curr _ Nil) = curr
		getMaxFromLeft (Node _ _ right) = getMaxFromLeft right

		--Ricostruisce l'albero
		rebuildLowerTree :: BSTree a -> BSTree a
			--Nel caso in cui si sia solo il figlio sinistro
		rebuildLowerTree (Node _ left Nil) = left
			--Nel caso in cui ci sia solo il figlio destro
		rebuildLowerTree (Node _ Nil right) = right
			--Nel caso ci siano entrambi i figli (rimuove la radice e la sostituisce)
		rebuildLowerTree (Node curr left right) = (Node (getMaxFromLeft (Node curr left right)) (removeMaxFromLeft left) right)

		--Rimuove il nodo sostituito alla radice
		removeMaxFromLeft :: BSTree a -> BSTree a
		removeMaxFromLeft (Node this thisLeft (Node curr Nil Nil)) = (Node this thisLeft Nil)
		removeMaxFromLeft (Node this thisLeft (Node curr left right)) = (Node this thisLeft (removeMaxFromLeft (Node curr left right)))

bstForEach :: (a -> IO ()) -> BSTree a -> IO ()
bstForEach forEachAction tree = bstFold (\x y -> x >> forEachAction y) (return()) tree

{-
Dati i tipi:
type Age = Int
data Person = Person String Age
Implementare la funzione:
printPers :: BSTree Person -> Int -> Int -> IO()
La funzione deve costruire  un’azione che stampi in ordine crescente i nomi di tutte  le persone appartenenti all’albero passato come primo parametro, la cui età sia compresa fra i valori specificati come secondo e terzo parametro.
-}

type Age = Int
data Person = Person String Age

personTree :: BSTree Person
personTree = (Node (Person "Enrico" 35)
			(Node (Person "Bruno" 15)
				(Node (Person "Anna" 7) Nil Nil)
				(Node (Person "Carlotta" 21) Nil  Nil))
			(Node (Person "Giacomo" 49)
				(Node (Person "Federico" 42) Nil Nil)
				Nil ))

printPers :: BSTree Person -> Int -> Int -> IO()
printPers tree lowerBound upperBound = bstForEach
											(\(Person name _) -> putStrLn name)
											(bstFilter (\(Person _ age) -> age >= lowerBound)
											(bstFilter (\(Person _ age) -> age <= upperBound) tree))


